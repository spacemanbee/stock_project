
import pandas as pd
from TW50KD import StockMarketData


class StockFilter():

    def __init__(self):
        self.path = "D:/googlecloud/StockData2/stocknum.csv" #我放在雲端資料夾裡

    def allstock(self):

        df = pd.read_csv(self.path)
        #讀取檔案裡的股票代號，弄成list形式['2330','xxxx'....]
        stocknum = list(map(lambda x : str(x), df["id"]))
        return stocknum

    def stocktable(self, year, month, day):

        # 這個函式是呼叫TW50KD裡的class然後整理成字典(key:股票代號, value: dataframe)
        SMD = StockMarketData()
        # SF = StockFilter()
        stocklist = self.allstock()
        temp = SMD.stock_table(year, month, day, stocklist)
        print(temp)
        input()
        return temp

    @staticmethod
    def add_1(input1):
        return input1+1

    def stock_filter_by_60_ma(self, year, month, day):

        # 這個函式是把季線往上的挑出來，其他刪去
        SF = StockFilter()
        tabledict = SF.stocktable(year, month, day)
        stock_is_filtered = []
        for stock, table in tabledict.items():
            slope1 = table['60MA'][-1] - table['60MA'][-2]
            slope2 = table['60MA'][-60] - table['60MA'][-40]
            slope3 = table['60MA'][-40] - table['60MA'][-20]
            if slope1 > 0 and slope2 < 0 and slope3 < 0:
                stock_is_filtered.append(stock)
            else:
                pass
        return stock_is_filtered

    def stockfilterbyprice(self, year, month, day):

        # 這函式把過去半年前股價和現在做比較(簡單說如果現在股價比起半年前股價過低的話就刪掉)
        SF = StockFilter()
        tabledict = SF.stocktable(year, month, day)
        stocklist = SF.stockfilterby60MA(year, month, day)
        newlist = []

        for stock in stocklist:
            pricelist = list(tabledict[stock]["close"][-120:-20])

            if max(pricelist) > tabledict[stock]["close"][-1] * 1.3:
                pass
            else:
                newlist.append(stock)
        print(len(tabledict), len(newlist))
        return newlist




SF = StockFilter()
print(SF.stockfilterbyprice(2018,5,5))