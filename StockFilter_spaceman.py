import pandas as pd
from TW50KD import StockMarketData
from StockPreProcessing import StockPreProcessing

# 虛男帥哥好，你寫的code可能可以跑，但是裡面存在不少問題跟多餘的地方，就透過修改這份code跟你慢慢講解
# 為了方便你對照，我會完整保留你原本寫的部分，就是直接註解然後自己寫新的
# 以下有幾個重點
# 1. 變數或函式命名問題，這其實不影響程式，只是當別人要看你的code的時候會比較好理解而已(朝著"讓別人看懂你程式"為目標來寫code是比較好的習慣)
# 每套程式語言都有自己的約定成俗的命名規則(通常python都用"PEP 8"這套規則，PyCharm裡面常常出現綠色底線就是說你沒按照PEP 8規則XD)，這個潛規則可以參考下列網址:
# http://note.drx.tw/2015/01/google-python-style-callname-rules-main.html
# 我自己習慣也不是很好，也在改進中，不過合作就是要互相統一格式然後一起建立好習慣
#
# 2. 雖然你已經用class包起來了，但你寫的程式看起來還是以傳統函式的方式在寫，我就在下面code直接實戰說明


# class的命名沒錯，是每個字的首字母大寫然後直接接起來，nice
# 繼續寫之前，應該先定義這個class應該要做些什麼
# 依照目前的設計，所有功能都會在MainStructure裡面上被執行
# 再加上設計的原則是做過的東西就不要再做一次了
# 所以原本的StockFilter.py裡面有重新做stocktable的事情，就不要重做，應想辦法從MainStructure把做過的table丟進來
# 所以在MainStructure的流程上會變成
# 1. 先把有興趣的stock_list讀進來(看起來你想要用讀stocknum.csv檔的方式)
# 2. 然後抓出所有stock_list的資料表，表裡面要有5ma 60ma那些東西
# 3. 根據上面的表才能過濾stock_list，產生新的stock_list
# 4. 執行原本程式
class StockFilter:
    # 寫物件導向程式的時候，要先想一下哪些"變數"和"功能"會在這個class以外的地方用到
    # 譬如以我來說，我如果想呼叫你寫的StockFilter類別時，直覺上，我就會期待看到
    # (1) 一開始輸入進去的股票清單，這邊會用讀檔的方式(所以也許給個檔案路徑) - input
    # (2) 清單內所有股票的資料 - input
    # (3) 選完後的結果清單 - output
    # (1)到(3)很明顯都是靜態的資料，所以個人建議應該要用成變數的形式，並弄成物件的public成員讓所有呼叫這個函數的人可以用
    # 你的版本的程式僅有path這個變數和4個函式，以python的規則而言，變數的宣告都會在__init__裡面，所以我會改成
    def __init__(self, stock_list_path):
        # 變數前面加一個底線表示這個類別以外的程式不需要用到，像這邊的path只是供內部讀檔用的(通常這類型的叫做private，反之有對外的東西都叫做public)
        # 呼叫這個類別的人(例如我)，其實不需要知道你把path設在哪裡，這樣一來當我呼叫StockFilter的時候就不會看到這個類別有提供這個變數使用
        # 這樣設計一來是怕別人改到你的路徑，二來其他人在用你的程式的時候也能夠看比較少資料(_path不會出現在這個class以外的提示裡面)
        # 不會有提示就是指你在外面呼叫SF = StockFilter()時，輸入"SF."後不會看到下拉式選單裡有_path這個變數，別人當然就改不到囉
        self._path = stock_list_path
        self.init_stock_list = self.get_init_stock_list()
        self.filtered_result = list()
        self.all_stock_table = None

    # def __init__(self):
    #     self.path = "D:/googlecloud/StockData2/stocknum.csv" #我放在雲端資料夾裡

    # PEP 8中，函式的命名跟變數的規定一樣，都是全字小寫用底線隔開，只是後面多了括號
    # 如果要再要求一點，在所有程式語言中，函式的命名第一個單字是動詞(因為函式可以想像成是動作，所以用動詞開頭，一般變數則都是名詞)
    def get_init_stock_list(self):
        # 這函式看起來只想讀檔後傳回list，所以如果沒有用到DataFrame的好處的話盡量不要用
        # 直接讀取成list就好了，類型轉換過多，程式效率會下降
        # 以下是原本程式，格式轉換流程為csv-->dataframe-->list
        # df = pd.read_csv(self.path)
        # stocknum = list(map(lambda x : str(x), df["id"]))
        # return stocknum
        # 第一個元素是'id'，所以最後不回傳rows[0]，不過我的寫法好像有點爛XD，也許有更簡潔的方法
        import csv
        with open(self._path, newline='') as csv_file:
            rows = csv.reader(csv_file, delimiter='\n')
            result = list(map(list, zip(*rows)))
            return result[0][1:]

    # def allstock(self):
    #     df = pd.read_csv(self.path)
    #     #讀取檔案裡的股票代號，弄成list形式['2330','xxxx'....]
    #     stocknum = list(map(lambda x : str(x), df["id"]))
    #     return stocknum
    # def get_stock_table(self, table):
        # 注意一下原本的函式，你在stocktable()都沒有用到self這個東西，但再仔細一看，SF竟然是呼叫自己
        # 原本的寫法會創建了一個新的StockFilter物件，與原本的物件無關，也就是說執行到這行(SF = StockFilter())會有兩個StockFilter物件
        # 這樣寫表示每執行一次stocktable()，就會產生一個新的StockFilter物件
        # 浪費了記憶體，這邊沒看懂的話再問我，頗重要的
        # 總之，這邊你想用上面寫的allstock()，就要用self才對，才不會一直創造新的物件
        # 另外TW50KD應該已經廢棄了才是，要改用StockPreProcessing.py才對吧，如果你覺得名稱不直覺，可以改
        # 但老實說這個table在MainStructure一開始執行的時候就會做了，
        # 這邊要用這個table的話應該是從MainStructure把table傳進來才對(可以看MainStructure.py的line 35和36)
        # SMD = StockMarketData()
        # stock_list = self.get_all_stock()
        # self.all_stock_table = table

    # def stocktable(self, year, month, day):

        # 這個函式是呼叫TW50KD裡的class然後整理成字典(key:股票代號, value: dataframe)
        # SMD = StockMarketData()
        # SF = StockFilter()
        # stocklist = SF.allstock()
        # return SMD.stock_table(year, month, day, stocklist)

# 以下盡量把額外創建的物件給刪掉，改用self處理
    def filter_by_ma_60(self):
        # 這個函式是把季線往上的挑出來，其他刪去
        # 應該要先根據年月日找到index 再-1, -60, -40才對，不然應該每次都會找一樣的東西出來
        for stock, table in self.all_stock_table.items():
            slope1 = table['MA_60'][-1] - table['MA_60'][-2]
            slope2 = table['MA_60'][-60] - table['MA_60'][-40]
            slope3 = table['MA_60'][-40] - table['MA_60'][-20]
            if slope1 > 0 > slope2 and slope3 < 0:
                self.filtered_result.append(stock)

    def filter_by_price(self, table):
        self.all_stock_table = table
        # 這函式把過去半年前股價和現在做比較(簡單說如果現在股價比起半年前股價過低的話就刪掉)
        self.filter_by_ma_60()
        newlist = []

        for stock in self.filtered_result:
            pricelist = list(self.all_stock_table[stock]["close"][-120:-20])

            if max(pricelist) <= self.all_stock_table[stock]["close"][-1] * 1.3:
                newlist.append(stock)
        print('過濾前:  ', self.init_stock_list)
        print('過濾一次:', self.filtered_result, '\n過濾二次:', newlist)
        self.filtered_result = newlist
