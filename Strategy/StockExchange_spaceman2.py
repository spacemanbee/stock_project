"""
這裡寫策略的人的任務更單純了
只要對每個有開市日期，產生哪支股票買賣張數就好，用dict表示
"""
from Exchange.ExchangeResult import ExchangeResult


class Strategy:
    def __init__(self, data_table, stock_list, ex):
        # 其實特徵應該要放在這裡，不應該放在MainStructure，但這個版本先這樣吧
        self.features = ['SMA', 'MACD']
        self.table = data_table
        self.stock_list = stock_list
        self.result = dict()
        self.ex = ex

    # 原則上寫策略的人要改這個地方，只要針對某一天寫對某支股票的策略就好
    def algo_for_one_stock_in_one_day(self, stock, idx):
        delta_5 = self.table[stock]['MA_5'][idx] - self.table[stock]['MA_5'][idx - 1]
        # delta_10 = table[stock]['10MA'][idx] - table[stock]['10MA'][idx - 1]
        delta_20 = self.table[stock]['MA_20'][idx] - self.table[stock]['MA_20'][idx - 1]
        # delta_60 = table[stock]['60MA'][idx] - table[stock]['60MA'][idx - 1]

        # 定義當天10日線與5日線的價差
        num = self.table[stock]['MA_10'][idx] - self.table[stock]['MA_5'][idx]

        # 判斷買入條件 : 買入金額小於戶頭擁有金額的20%，而且20日線往上成長
        # 這樣只會買一次而不是20日線往上的時候一直買
        if delta_20 > 0:
            # 用開盤價買進來算
            bprice = self.table[stock]['close'][idx]
            # 買入張數為二十萬/(股價*1000)
            test = ExchangeResult('富邦')
            buy_num = test.max_number_can_buy(self.ex.money_quota, bprice)
            return buy_num

        # 賣出條件 : 如果 1.我有持股 2.20日線價差小於2%的當日20日價格 3. 5日線開始向下
        #             4. 10日與5日價差低於當日10日線價格的3%
        elif delta_20 < 0.02 * self.table[stock]['MA_20'][idx] and delta_5 < 0 and num < 0.03 * self.table[stock]['MA_10'][idx]:
            sell_num = 0
            if stock in self.ex.hold_table.keys():
                sell_num = self.ex.hold_table[stock]
            return -sell_num
        else:
            return 0

    # 這裡會幫忙跑在股票清單內的所有股票，日期的迴圈在MainStructure，這裡也會幫忙寫成dict，正常情況下不用改下面程式
    def run(self, date):
        # 這裡要先檢查date是否有對到date_idx，不是就不做
        for stock in self.stock_list:
            idx_list = list(self.table[stock].index)
            if date not in idx_list:
                self.result[stock] = 0
                continue
            # 找date對應的index
            idx = idx_list.index(date)
            self.result[stock] = self.algo_for_one_stock_in_one_day(stock, idx)
        return self.result
