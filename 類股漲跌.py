"""
All fields of trade data, we can search the transaction quantity, transaction amount
and daily pricing limits

"""

import requests
import datetime, time
import json
import pandas as pd


today_date = datetime.datetime.now()
YY = today_date.year
MM = today_date.month
DD = today_date.day

url = "http://www.twse.com.tw/exchangeReport/BFIAMU?response=json&date=20181126"

def data_of_all_fields(URL):

    """
     Input
     - URL :  the linkage of data resource

     Output
     - A dictionary of all fields

    """

    html = requests.get(URL)
    js = json.loads(html.text)
    if js['stat'] == 'OK':
        try:
            index_dict = {}
            for row in js['data']:
                index_dict[row[0].rstrip(' ')] = row[1:]

            for key, value in index_dict.items():
                list = []
                for idx in value:
                    b = idx.replace(',' , '')
                    list.append(b)
                index_dict[key] = list
            return index_dict

        except:
            return ""

#print(data_of_all_fields(url))

def one_field_data_for_one_period(year, month, day):

    '''
     Input:
     - year : the year we wuold like to catch data from
     - month : the month we wuold like to catch data from
     - day : the day we wuold like to catch data from

     Output:
     - A list includes the selfvander investment number

    '''

    year_list = list(range(year, YY + 1))
    month_list = list(range(month, MM + 1))
    day_list = list(range(day, DD + 1))


    for years in year_list:
        for months in month_list:
            for days in day_list:

                if datetime.date(years, months, days) == datetime.datetime.now():
                    break

                else:
                    url = ("http://www.twse.com.tw/exchangeReport/BFIAMU?response=json&date="
                           + str(years) + str(months) + str(days))
                    datatext = data_of_all_fields(url)
                    dataform = (pd.DataFrame(datatext)).T
                    #print(dataform)
                    time.sleep(5)

#print(one_field_data_for_one_period(2018, 11, 26))













