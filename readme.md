# 本專案所需套件及開發版本
1. Python 3.7.0 64位元
2. twstock
3. TA-Lib (64位元windows作業系統請參考 #安裝TA-Lib)

# 安裝TA-Lib (適用於64位元windows作業系統)
1. 請確保python是安裝64位元的版本
2. 如果是python 3.7，請跳至下一步，否則請至https://www.lfd.uci.edu/~gohlke/pythonlibs/#ta-lib 下載對應版本的whl，然後放到主目錄
3. 請同步本專案，在主目錄下會有個檔案叫做TA_Lib-0.4.17-cp37-cp37m-win_amd64.whl (whl是python套件包，可以用pip install xxx.whl來安裝)
4. 開啟有本專案venv的terminal，並輸入pip install TA_Lib-0.4.17-cp37-cp37m-win_amd64.whl

# 定時器的用法(讓一個function能夠定時或週期性的重複執行)
1. 假設有個函式為func(a, b, c)，想讓這函式每天下午4點執行一次
2. 匯入import TimeActivator，並宣告一物件 ta = TimeActivator()
3.輸入ta.re_run_func_by_daytime("16:00:00", func, [a, b, c])，注意func和abc的輸入格式
4. 再輸入ta.activate()啟動定時器
5. ta.activate()只需要呼叫一次就好，之後若要再定時執行其他function，只要用3的方式即可

# 取得過去所有股票K線資料
1. 執行PastData.py(注意路徑設定及抓取的資料日期)