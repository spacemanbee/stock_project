import twstock
import csv
import numpy
import talib
from twstock import Stock
import datetime
import time
from exchange import ExchangeResult
import urllib.parse
from collections import namedtuple
try:
    from json.decoder import JSONDecodeError
except ImportError:
    JSONDecodeError = ValueError

import requests
DATATUPLE = namedtuple('Data', ['date', 'open', 'high', 'low', 'close'])


class TWSEFetcherForIndexKLine(twstock.stock.BaseFetcher):
    # REPORT_URL = urllib.parse.urljoin(TWSE_BASE_URL, 'exchangeReport/STOCK_DAY')

    def __init__(self):
        self.REPORT_URL = 'http://www.twse.com.tw/indicesReport/MI_5MINS_HIST?response=json'

    def fetch(self, year: int, month: int, sid: str, retry: int=5):
        params = {'date': '%d%02d01' % (year, month)}
        for retry_i in range(retry):
            r = requests.get(self.REPORT_URL, params=params)
            try:
                data = r.json()
            except JSONDecodeError:
                continue
            else:
                break
        else:
            # Fail in all retries
            data = {'stat': '', 'data': []}

        if data['stat'] == 'OK':
            data['data'] = self.purify(data)
        else:
            data['data'] = []
        return data

    def _make_datatuple(self, data):
        data[0] = datetime.datetime.strptime(self._convert_date(data[0]), '%Y/%m/%d')
        data[1] = None if data[1] == '--' else float(data[1].replace(',', ''))
        data[2] = None if data[2] == '--' else float(data[2].replace(',', ''))
        data[3] = None if data[3] == '--' else float(data[3].replace(',', ''))
        data[4] = None if data[4] == '--' else float(data[4].replace(',', ''))
        return DATATUPLE(*data)

    def purify(self, original_data):
        return [self._make_datatuple(d) for d in original_data['data']]


class TWSEIndex(Stock):
    def __init__(self, initial_fetch: bool=False):
        self.sid = '9999'
        self.fetcher = TWSEFetcherForIndexKLine()
        self.raw_data = []
        self.data = []

        # Init data
        if initial_fetch:
            self.fetch_31()


dataBase = {}
bigDisk = TWSEIndex()
bigDisk.fetch_from(2018, 10)
dataBase['9999'] = bigDisk
print(bigDisk.open)
print(bigDisk.close)
print(bigDisk.high)
print(bigDisk.low)
time.sleep(3)
tw50 = Stock('0050')
tw50.fetch_from(2018, 10)
dataBase['0050'] = tw50
print(tw50.open)
print(tw50.close)
print(tw50.high)
print(tw50.low)

# 這裡開始測試talib的功能
sma = talib.SMA(numpy.array(tw50.close))
with open('output.csv', 'w', newline='') as csvfile:
    csvw = csv.writer(csvfile)
    csvw.writerow(tw50.close)
    csvw.writerow(sma)

test = ExchangeResult('1040')
# for i in bigDisk.low:
#     if bigDisk.open[i] - bigDisk.low[i] <= 20:
#         test.buy_stock('0050', bigDisk.date[i], 1)

