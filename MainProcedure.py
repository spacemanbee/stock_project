from TimeActivator import TimeActivator
from RealTimeStock import RealTimeStock
from Exchange import ExchangeResult
# 設定每天啟動時間，與停止時間，大概設定在開盤前後兩分鐘
# 啟動後，會每隔三秒去抓targets.csv裡面的代碼(這部分寫在RealTimeStock.py裡面)
# 停止後，會觸發存檔，因為一般來說電腦儲存檔案的速度過慢(跟記憶體存取來比)，所以每天收盤存一次
start_time = "08:58:00"
stop_time = "13:32:00"

# 這邊宣告定時器和即時資料物件
timer = TimeActivator(start_time, stop_time)
r_t_stock = RealTimeStock()


# 先定義每3秒執行即時資料抓取的函式
def periodic_get_real_time_data():
    timer.re_run_func_by_time_interval(r_t_stock.get_real_time_stock, [])


def save_and_initialize_data():
    r_t_stock.save_real_time_data()
    r_t_stock.initial_real_time_data()


# # 每天在start_time會執行上面函式
# timer.re_run_func_by_daytime(start_time, periodic_get_real_time_data, [])
# # 每天在stop_time會觸發存檔
# timer.re_run_func_by_daytime(stop_time, save_and_initialize_data, [])
# # 測試用，底下那行會列出目前timer裡面的task
# # timer.re_run_func_by_daytime(stop_time, timer.show_timer_content, [])
#
# # 啟動定時器
# timer.activate()

ex = ExchangeResult.ExchangeResult('富邦')
print(ex.total_income_to_sell_stock(500))
print(ex.total_pay_to_buy_stock(500))