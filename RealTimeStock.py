import twstock
import TimeActivator
import re
import csv
import json
from datetime import date


class RealTimeStock:
    # 理論上每個物件都要有這個，當在創立新物件的時候會自動執行__init__
    def __init__(self):
        # 這裡放目前要追蹤的股票代碼清單檔名(含路徑)，因為檔案剛好放在同一層，所以直接寫檔名
        self.TARGET_STOCK_PATH = 'targets.csv'

        # 這裡要放跟google雲端同步的資料夾路徑
        self.REAL_TIME_DATA_ROOT = 'D:/GoogleCloud/StockData2/RealTimeStock/'

        # 我想建立target股票代碼list、找其中為4位數字組成的，所以目前target.csv裡5G的字樣會被忽略
        # 這裡用regular expression的方式來過濾不是四位數字的list member
        # REGEX: ^就是指開頭，$代表結尾，\d代表數字，{4}代表個數(數字開頭，數字結尾，且共四個數字的所有字串)
        self.REGEX = re.compile("^\d{4}$")

        # 讀取target.csv，這裡面也會實作過濾四位數字的功能
        self.target_codes = []
        self.read_target_stocks_from_file()

        # 因為要弄成dictionary，所以先將targets.csv中的股票代碼弄成該dictionary的key
        # real_time_data裡面最後會儲存所有目標代碼的即時資料，格式如下
        # "4906": {"1547188200.0": ["25.10", "417", "5184"]}
        # <代碼>: {<time stamp>: [<latest_trade_price>, <trade_volume>, <accumulate_trade_volume>]}
        self.real_time_data = {}
        self.initial_real_time_data()

        # 這是測試用，不重要
        # self.test_counter = 0

    def read_target_stocks_from_file(self):
        with open(self.TARGET_STOCK_PATH, newline='') as csv_file:
            # 讀取 CSV 檔案內容
            rows = csv.reader(csv_file)
            for row in rows:
                self.target_codes.append(row[0])
            # 取得所有股票代碼、建立代碼list、找其中為4位數字組成的
            # 這裡用regular expression的方式來過濾不是四位數字的list member
            self.target_codes = list(filter(self.REGEX.match, self.target_codes))

    # 初始化dict
    def initial_real_time_data(self):
        self.real_time_data = {}
        for stock_id in self.target_codes:
            self.real_time_data[stock_id] = {}

    def get_real_time_stock(self):
        stocks = twstock.realtime.get(self.target_codes)
        # print(stocks)
        # print(stocks.keys())
        keys = list(stocks.keys())[:-1]
        # 這邊要排除最後一個key('success')是bool型態，不然會有例外產生，[:-1]就是陣列從開頭到倒數第二個的意思
        for code in keys:
            # 有抓成功才寫入，這裡也盡量縮小儲存所需的字數(為了節省空間)，因為爬下來的json有太多重複字串
            if stocks[code]['success']:
                time_stamp = stocks[code]['info']['time'][-8:]
                # 這裡檢查是否抓到的資料是開盤前無值的狀態還是前一天收盤的資料
                # 14:30:00表示是前一天收盤資料，無值的話表示當天有開盤，但時間還沒到
                if time_stamp == "14:30:00" or stocks[code]['realtime']['latest_trade_price'] is None:
                    continue
                # time_stamp = str(stocks[code]['timestamp'])
                stock_real_time = [stocks[code]['realtime']['latest_trade_price'], stocks[code]['realtime']['trade_volume'], stocks[code]['realtime']['accumulate_trade_volume']]
                self.real_time_data[code][time_stamp] = stock_real_time
        # self.test_counter += 1
        # print(self.test_counter, self.real_time_data)

    # 這裡用日期當作檔名，只在MainProcedure裡呼叫
    def save_real_time_data(self):
        today = date.today().strftime("%Y%m%d")
        file_name = self.REAL_TIME_DATA_ROOT + str(today) + ".json"
        print(today, '寫入完成')
        with open(file_name, "w") as f:
            json.dump(self.real_time_data, f)


# 流程大概是
# 排入一個早上8:58分的work，該work內容是每3秒執行get_real_time_stock直到13:32
