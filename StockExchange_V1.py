"""
stock buyin and sellout analysis


"""

import datetime
from ExchangeResult import ExchangeResult
from TW50KD import StockMarketData
import pandas as pd


today = datetime.datetime.now()
YY = today.year
MM = today.month
DD = today.day

#用富邦的戶頭做證券交易
ex = ExchangeResult("富邦")

def stocklist():

    df = pd.read_csv("stocknum.csv")
    stocknum = []
    for idx in df["id"]:
        stocknum.append(str(idx))
    return stocknum


def tech_exchange(year, month, day, stocklist):


    #呼叫想追蹤股的資料，stocklist = ["XXXX","XXXX","XXXX"]
    SMD = StockMarketData()
    table = SMD.stock_table(year, month, day, stocklist)

    for stock in table.keys():
        total_money = 1000000 #假設戶頭裡有100萬
        my_stock_num = 0  # 初始持股數量是0
        buyinmoney = 0  #尚未買入股票，買入金額是0

        for idx in range(len(table[stock]['close'])):

            #定義各日線當天與前一天價差
            delta_5 = table[stock]['5MA'][idx] - table[stock]['5MA'][idx - 1]
            #delta_10 = table[stock]['10MA'][idx] - table[stock]['10MA'][idx - 1]
            delta_20 = table[stock]['20MA'][idx] - table[stock]['20MA'][idx - 1]
            #delta_60 = table[stock]['60MA'][idx] - table[stock]['60MA'][idx - 1]

            #定義當天10日線與5日線的價差
            num = table[stock]['10MA'][idx] - table[stock]['5MA'][idx]

            #判斷買入條件 : 買入金額小於戶頭擁有金額的20%，而且20日線往上成長
            #這樣只會買一次而不是20日線往上的時候一直買
            if buyinmoney < 0.2 * total_money and delta_20 > 0:

                # 用開盤價買進
                bprice = table[stock]['close'][idx]
                #買入張數為二十萬/(股價*1000)之後再多買一張
                stock_buyin_for_once = (int(total_money * 0.2 / (bprice * 1000))) + 1
                #買入金額就是買入一張的金額加上手續費乘上買入張數
                buyinmoney = buyinmoney + ex.total_pay_to_buy_stock(bprice) * stock_buyin_for_once
                #戶頭用有的就是100萬減去買入金額
                total_money -= buyinmoney
                #我的目前持股張數
                my_stock_num += stock_buyin_for_once

            #賣出條件 : 如果 1.我有持股 2.20日線價差小於2%的當日20日價格 3. 5日線開始向下
            #             4. 10日與5日價差低於當日10日線價格的3%
            elif (my_stock_num != 0 and delta_20 < 0.02 * table[stock]['20MA'][idx] and
                      delta_5 < 0 and num < 0.03*table[stock]['10MA'][idx]):
                # 用開盤價賣出
                sprice = table[stock]['open'][idx]
                #買入金額歸0
                buyinmoney = 0
                #戶頭總金額為目前擁有加上一張金額賣出價格減去手續費之後再乘上張數
                total_money += ex.total_income_to_sell_stock(sprice) * my_stock_num
                # 持股歸零
                my_stock_num = 0

            else:
                pass

        #這裡判斷是為了計算損益，假設目前有買入股票但是還沒賣出，會把買入價格退還給戶頭擁有的價格
        #如果要計算獲利百分比應該是(賣出金額-買入金額)*100%/買入金額
        #現在是先計算戶頭裡總共多了多少$$

        if my_stock_num != 0:
            total_money += buyinmoney
            print("The profit of " + str(stock) + " is " + str(total_money - 1000000))
        else:
            print("The profit of " + str(stock) + " is " + str(total_money - 1000000))
    return ""


#這是想追蹤的股票
stocklist = stocklist()
print(tech_exchange(2018,2,6,stocklist))

#--------------------------------------------------------------------------------------------------
# def trade_comparison(num):
#
#     """
#      Input:
#       - num : stock id for searching
#
#      Output:
#       - three lists are output, first list contains the price that big custom buys the large
#        quantites of stock, second list contains the price that big custom sells the stock,
#        the third one is the length difference between the first and second list.
#
#     """
#
#     mybuyin = []
#     mysoldout = []
#     buyin_cnt = 0
#     sellout_cnt = 0
#
#     for i in range(100):
#         realtimedata = realtime.get(num)
#         tradevolumn = float(realtimedata['realtime']['trade_volume'])
#         tradeprice = float(realtimedata['realtime']['latest_trade_price'])
#         bestfivebidprice = float(realtimedata['realtime']['best_bid_price'][0])   # buyin price
#         bestfiveaskprice = realtimedata['realtime']['best_ask_price'][0]   # sellout price
#         bigdcustom = BIG_NTD / (float(tradeprice)*1000)   # the quantities that big custom have
#
#         if float(tradevolumn) >= bigdcustom:    # bigcustom is coming
#             if tradeprice > bestfivebidprice:
#
#                 print('total volumn: ', tradevolumn , end = '  ')
#                 print('price: ',  tradeprice)
#                 print('result: buyin')
#
#                 mybuyin.append(tradeprice)
#                 buyin_cnt += 1
#                 time.sleep(5)
#             else:
#                 if buyin_cnt == 0:
#                     pass
#                     time.sleep(2)
#                 else:
#                     print('total volumn: ', tradevolumn, end='  ')
#                     print('price: ', tradeprice)
#                     print('result: sellout')
#
#                     mysoldout.append(tradeprice)
#                     sellout_cnt += 1
#                     time.sleep(5)
#
#         else:
#             print('total volumn: ', tradevolumn)
#             print('result: nothing')
#             time.sleep(5)
#
#     return mybuyin, mysoldout, (buyin_cnt - sellout_cnt)

# TW50 = Tw50KDData()
# print(trade_comparison(stocknum))

# def kd_comparison(year, month, day, stocklist):
#
#     # dt = date(year, month, day)
#     # step = datetime.timedelta(days=1)
#     SMD = StockMarketData()
#     df = SMD.stock_table(year, month, day, stocklist)
#
#     for key,stock in df.items():
#         pricedict = {}
#         MY_STOCK_NUM = 0  # 初始持股數量是0
#
#         for idx in range(len(stock.k_value)):
#
#             if stock.k_value[idx] == np.nan:
#                 pass
#             elif stock.k_value[idx] <= MIN_K: # 當k值低於下限值
#                 bprice = stock.close[idx] # 目前設定買進價格為當天的收盤價
#                 pricedict[stock.Date[idx]] = (bprice, 'buy')
#                 MY_STOCK_NUM += 1 # 買進增加持股
#             elif stock.k_value[idx] >= MAX_K and MY_STOCK_NUM > 0: # 當k值高於上限值且我有持股
#                 sprice = stock.close[idx] # 目前設定賣出價格為當天的收盤價
#                 pricedict[stock.Date[idx]] = (sprice, 'sell')
#                 MY_STOCK_NUM = 0 # 目標是一次賣光手上持股，最後持股是0
#             else:
#                 pass
#
#         buymoney = 0
#         soldmoney = 0
#         stock_num_i_have = 0
#         tansaction_num = 0
#         for value in pricedict.values():
#             if value[1] == 'buy':
#                 buymoney += value[0]*1000*(STOCK_BUYIN_FOR_ONCE) # 買入股票花費的金額
#                 stock_num_i_have += STOCK_BUYIN_FOR_ONCE # 買入張數
#                 tansaction_num += 1 # 交易多一筆
#             else:
#                 soldmoney += value[0]*1000*(stock_num_i_have) # 一次賣掉手上的持股乘上金額
#                 stock_num_i_have = 0 # 持股歸0
#                 tansaction_num += 1 # 交易多一筆
#
#         print("The "+key+" earned "+ str(soldmoney - buymoney)+" dollars") # 計算賺與賠的差異
#         print(tansaction_num) # 成交次數
#
#     return ""
#
# def bb_exchange(year, month, day, stocklist):
#
#     MY_STOCK_NUM = 0 # 初始持股數量是0
#     BB = BooleanBand()
#     table = BB.booleanbands(year, month, day, stocklist) # stocklist可以是我們想追蹤的個股
#
#     for stock in table.keys(): # 就是股票代號
#         pricedict = {}
#         for idx in range(len(table[stock]['close'])):
#
#             if table[stock]["bbupper"][idx] == 0: #前19天都沒有data(因為是第20天才會有data)
#                 pass
#
#             elif table[stock]['open'][idx] < table[stock]["bblower"][idx]: #當開盤價低於布林通道下限值
#                 bprice = table[stock]['open'][idx] #用開盤價買進
#                 #將資料整理成字典格式,key是買進的日期, value是tuple(買進價格,買的動作)
#                 pricedict[table[stock]['Date'][idx]] = (bprice, "buy")
#                 MY_STOCK_NUM += 1 #持股多一張
#
#             #當有持股且開盤價和收盤價都高於布林通道上限值
#             elif (MY_STOCK_NUM > 0 and table[stock]['close'][idx] > table[stock]["bbupper"][idx] and
#                     table[stock]['open'][idx] > table[stock]["bbupper"][idx]):
#                 #用開盤價買進
#                 sprice = table[stock]['open'][idx]
#                 #將資料整理成字典格式, key是賣出的日期, value是tuple(賣出價格, 賣的動作)
#                 pricedict[table[stock]['Date'][idx]] = (sprice, "sell")
#                 MY_STOCK_NUM = 0 #持股歸零
#             else:
#                 pass
#
#
#         buymoney = 0
#         soldmoney = 0
#         stock_num_i_have = 0
#         tansaction_num = 0
#         # bpricelist = []
#         # spricelist = []
#         for value in pricedict.values():
#             if value[1] == 'buy':
#                 buymoney += value[0]*1000*(STOCK_BUYIN_FOR_ONCE) # 買入股票花費的金額
#                 stock_num_i_have += STOCK_BUYIN_FOR_ONCE # 買入張數
#                 tansaction_num += 1 # 交易多一筆
#             else:
#                 soldmoney += value[0]*1000*(stock_num_i_have) # 一次賣掉手上的持股乘上金額
#                 stock_num_i_have = 0 # 持股歸0
#                 tansaction_num += 1 # 交易多一筆
#
#         print(soldmoney - buymoney) # 計算賺與賠的差異
#         print(tansaction_num) # 計算交易筆數
#
#     return ""

