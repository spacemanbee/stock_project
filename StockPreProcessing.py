"""
 KD line of TW50, find the difference of kd data to
decide when is the best buyin point

"""
from os import listdir
from talib import abstract
import datetime
import time
import pandas as pd
from datetime import date
import json
import talib

today = datetime.datetime.now()
YY = today.year
MM = today.month
DD = today.day


class StockPreProcessing:

    def __init__(self, year, month, day, stock_list):
        self.TA_DICT = {'SMA': {'funcName': self.SMA, 'content': ['MA_5', 'MA_10', 'MA_20', 'MA_60']},
                        'MACD': {'funcName': self.MACD, 'content': ['MACD', 'MACDsignal', 'MACDhist']}}
        self.STEP = datetime.timedelta(days=1)
        self.URL = ("https://goodinfo.tw/StockInfo/ShowK_Chart.asp?"
                    "STOCK_ID=%E5%8A%A0%E6%AC%8A%E6%8C%87%E6%95%B8&CHT_CAT2"
                    "=DATE=20190117")
        self.COLUMN = ["open", "close", "high", "low", "capacity"]
        self.MAX_HISTORY_SIZE = 90
        # 這一個是抓取大盤資訊的URL,這裡暫時用不到，不過先留著
        #self.connect = "https://finance.yahoo.com/quote/%5ETWII/history?p=%5ETWII"

        # 這行是先把之前抓取的資料找出想追蹤的股票之後，整理成json格式

        self.start_date = date(year, month, day)
        self.path1 = "D:/GoogleCloud/"  #sheman's path
        self.path2 = "StockData2/"      #sheman's path
        self.path3 = "stockdata/"       #sheman's path
        # 以後用相對路徑好了，免得改來改去
        self.relative_path = self.path1 + self.path2 + self.path3

        self.stock_list = stock_list
        # self.stockjson = {}
        self.file_list = list()
        self.active_date = list()
        self.stock_json = self.load_stock_data()
        self.stock_name = self.get_stock_name(stock_list)
        # print(self.stock_name)
        # input()
        # print(self.stock_json)

    @staticmethod
    def is_valid_date(date_str):
        try:
            d = time.strptime(date_str, "%Y%m%d")
            dt = date(d.tm_year, d.tm_mon, d.tm_mday)
            return True, dt
        except:
            print('檔名有誤', date_str)
            return False, 0

    def get_stock_name(self, stock_list):
        # 這個函式必須在load_stock_data()後面執行
        stock_name = dict()
        for i in self.file_list:
            with open(self.relative_path + i, "r", encoding="utf-8") as f:
                js = dict(json.load(f))['stockname']
            complete = True
            for j in stock_list:
                if j not in js.keys():
                    complete = False
                else:
                    stock_name[j] = js[j]
            if complete:
                break
        return stock_name

    def load_stock_data(self):
        # 改進讀取效率，增加容錯率
        self.file_list = listdir(self.relative_path)
        self.file_list = list(filter(lambda x: len(x) == 13, self.file_list))
        # print(file_list)
        filename = self.start_date.strftime("%Y%m%d") + ".json"
        # print(filename)
        try:
            idx = self.file_list.index(filename)
        except ValueError:
            copy_list = self.file_list.copy()
            copy_list.append(filename)
            copy_list.sort()
            idx = copy_list.index(filename)
            # print(idx)
            idx = max(idx - self.MAX_HISTORY_SIZE, 0)

        max_idx = len(self.file_list)
        # 初始化
        stock_json = dict()
        for j in self.stock_list:
            stock_json[j] = dict()
        while idx < max_idx:
            date_str = self.file_list[idx][:-5]
            is_date, dt = self.is_valid_date(date_str)
            if is_date:
                if dt >= self.start_date:
                    self.active_date.append(dt)
                try:
                    f = open(self.relative_path + self.file_list[idx], "r", encoding="utf-8")
                    js = json.load(f)
                    # print(js)
                    for j in self.stock_list:
                        # 不能保證stocklist一定每天都有交易(有時候會遇到暫停交易)
                        # 20160330的鴻海(2317)有"--"的數值(新聞說暫停交易due to 收購sharp，但key仍有2317，只是數據是'--')
                        # 減資的時候倒是連key都消失了，所以兩個case都要處理
                        if j not in js[self.COLUMN[0]].keys():
                            continue
                        if js[self.COLUMN[0]][j] == '--':
                            continue
                        price_dict = dict()
                        for item in self.COLUMN:
                            # 把開盤收盤最高最低價全部整理dictionary,並且把數字裡的逗號拿掉
                            price_dict[item] = float(js[item][j].replace(",", ""))
                        stock_json[j][dt] = price_dict
                except:
                    print('可能會是讀檔的問題', date_str)
                    pass
            idx += 1
        return stock_json

    def tech_analysis(self, stock, tech_list):
        # 這裡要可以吃所有技術分析的term並轉成我們要的格式
        # 轉成dataframe形式，將資料矩陣作轉秩動作
        df = pd.DataFrame(self.stock_json[stock]).T
        # 將date裡的值從字串轉成datetime，如果要畫圖的話matplotlib就可以直接讀取
        df['Date'] = pd.to_datetime(df.index)
        for tech in tech_list:
            result = self.TA_DICT[tech]['funcName'](df)
            df = pd.concat([df, result], axis=1, join_axes=[df.index])
        # df.to_csv('tttt.csv', sep=',', encoding='utf-8')
        df.dropna(how="any", inplace=True)  # 把dataframe裡的Na值全部改成0
        # 整理成開始日期以後的資料
        df = df[df['Date'] >= pd.Timestamp(self.start_date)]
        return df

    def SMA(self, df):
        # 這裡要找出5日線,10日線,20日線
        res = pd.DataFrame()
        for i in self.TA_DICT['SMA']['content']:
            feature_name, para = i.split('_', -1)
            res[i] = abstract.SMA(df, int(para))
        # ma5 = abstract.SMA(df, 5)
        # ma10 = abstract.SMA(df, 10)
        # ma20 = abstract.SMA(df, 20)
        # ma60 = abstract.SMA(df, 60)

        # res['MA_5'] = ma5
        # res['MA_10'] = ma10
        # res['MA_20'] = ma20
        # res['MA_60'] = ma60
        # 在dataframe中多加入3行
        return res

    def MACD(self, df):
        res = pd.DataFrame()
        # res['Date'] = df.index
        # 取得MACD技術資料
        res["MACD"], res["MACDsignal"], res["MACDhist"] = (talib.MACD(df["close"], fastperiod=12,
                                                                   slowperiod=26, signalperiod=9))
        return res

    def get_tech_analysis(self, tech_list):
        table_dict = {}
        for stock in self.stock_list:
            table_dict[stock] = self.tech_analysis(stock, tech_list)
        return table_dict

    # def stock_SMA_MACD(self):
    #
    #     """
    #      Input:
    #      - year : the year of historic data we catch
    #      - month : the month of historic data we catch
    #      - day : the day of historic data we catch
    #      - stocklist : 我們想追蹤的股票代碼(str)
    #
    #      Output:
    #      - the dataframe of stock we choose, including all the information
    #
    #     """
    #
    #     table_dict = {}
    #     # SMD = StockMarketData()
    #
    #     # 呼叫function的json格式資料
    #     # datatable = SMD.stock_data_json(year, month, day, stocklist)
    #
    #     for stock in self.stock_list:
    #
    #         # 轉成dataframe形式，將資料矩陣作轉秩動作
    #         df = pd.DataFrame(self.stock_json[stock]).T
    #
    #         # column多加了Date一行
    #         df['Date'] = df.index
    #
    #         #將date裡的值從字串轉成datetime，如果要畫圖的話matplotlib就可以直接讀取
    #         df['Date'] = pd.to_datetime(df['Date'])
    #
    #         #這裡要找出5日線,10日線,20日線
    #         ma5 = abstract.SMA(df, 5)
    #         ma10 = abstract.SMA(df, 10)
    #         ma20 = abstract.SMA(df, 20)
    #         ma60 = abstract.SMA(df, 60)
    #
    #         #在dataframe中多加入3行
    #         df['MA_5'] = ma5
    #         df['MA_10'] = ma10
    #         df['MA_20'] = ma20
    #         df['MA_60'] = ma60
    #
    #         #取得MACD技術資料
    #         df["MACD"], df["MACDsignal"], df["MACDhist"] = (talib.MACD(df["close"], fastperiod=12,
    #                                                                  slowperiod=26, signalperiod=9))
    #         df.dropna(how="any", inplace=True)  # 把dataframe裡的Na值全部改成0
    #
    #         # key是股票代碼(str), value是dataframe形式的股票資料
    #         table_dict[stock] = df
    #     # df.to_csv('tttt123.csv', sep=',', encoding='utf-8')
    #
    #     return table_dict


# a = datetime.datetime.now()
# test = StockPreProcessing(2016, 1, 2, ['2330', '0050', '2317'])
# print(test.stock_SMA_MACD())
# print(datetime.datetime.now()-a)
