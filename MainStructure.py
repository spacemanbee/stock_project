# 這裡想要蓋一個通用架構
# 讓各種不同的Algo可以用的架構
# 每個algo應該只要提供一個dict，內容只要包含買什麼股票幾張
# 例如{"2330": 5, "0050": 10} 就是買2330五張和0050十張的意思
# {"2330": -5, "0050": -1}就是賣5張和1張的意思
# 回傳{"2330": 0, "0050": 0}就是不買不賣
# 讀檔和日期應該包含在此架構內，讓algo設計者可以專心在制定買賣策略

from Exchange.ExchangeAction import ExchangeAction
from StockPreProcessing import StockPreProcessing
from StockFilter_spaceman import StockFilter
import importlib
# 先把要input的資料全部準備好
# 要準備的資料包含
# 1. 要追蹤的股票K線
# 2. 特徵值(例如5MA, 10MA, MACD等訊息)


class MainStructure:
    def __init__(self, year, month, day, path, total_money, broker, strg_version):
        # 這裡要列出所有的策略(strategy)，目前先用手動加入，未來應該可用掃資料夾的方式
        # 這邊填入Algorithm資料夾裡面的檔案，每個檔案都是一個策略
        # 譬如說StockExchange_spaceman2就代表一個策略(就是用StockExchange去改的，規則應該一樣)
        # v01就是代表別稱，未來可能會需要同時測試多個策略，或是同時讓多個策略決定後再來交易，這邊以後再說
        # 目前test沒用，總之以後有新的策略就放在 Strategy 裡面吧
        self.stock_list_path = path
        self.filter = StockFilter(self.stock_list_path)
        self.strg_module_name = {'v01': 'StockExchange_spaceman2',
                                 'v02': 'test'}
        # 這裡就用別稱來呼叫是哪一個策略
        self.strg_selection = strg_version

        # 這邊呼叫做資料預處理的功能，以後應該會專門開一個preprocessing的資料夾，裡面放所有可能需要的預處理功能
        # get_tech_analysis功能會統一處理輸入清單裡面的元素(就是指'SMA'和'MACD')，會跟原始raw data合併後回傳
        # 有興趣可以自己print一下table_feature看看
        # 他只會計算輸入清單裡面有寫的，這樣做也可以節省計算量，就是只算這個策略需要的而已
        self.spp = StockPreProcessing(year, month, day, self.filter.init_stock_list)
        self.table_features = self.spp.get_tech_analysis(['SMA', 'MACD'])
        self.filter.filter_by_price(self.table_features)

        # 最後呼叫之前寫好的股票買賣功能，就是.buy和.sell，然後會自動寫進交易紀錄的功能
        self.ex = ExchangeAction(broker, total_money)

    def activate(self):
        # 這裡用動態import的方式來import所選的策略
        # 以後策略會弄成一個abstract class(這個字可能有點難懂，不過程式設計上是這樣叫的，應該可以理解為空框架，寫策略的人就負責填空就好)
        # https://www.ycc.idv.tw/introduction-object-oriented-programming_2.html
        # 轉到下面有個"抽象化：抽象類別(Abstract Class)、抽象方法(Abstract Method)和接口(Interface)"的項目
        # 看不懂就先跳過吧XD
        # 總之，未來寫策略就只要填空即可，這邊的stg就是宣告的策略物件
        strg = importlib.import_module('Strategy.' + self.strg_module_name[self.strg_selection])
        stg = strg.Strategy(self.table_features, self.filter.filtered_result, self.ex)

        # 這裡開始對有開盤的日期進行策略模擬
        # 理論上每個策略裡面都會有個.run，去看Strategy/StockExchange_spaceman2.py吧
        # 然後目前把買賣也都從策略分離出來了，現在每個策略只要說哪個股票買幾張就好，買價賣價這個程式會自己去查
        for date in self.spp.active_date:
            result_date = stg.run(date)
            self.buy_sell_based_on_result(result_date, date)

        # 最後把交易紀錄寫成檔案
        print('跑完存檔囉')
        self.ex.save_file()

    def buy_sell_based_on_result(self, result, date):
        for stock in result.keys():
            if result[stock] == 0:
                continue
            elif result[stock] > 0:
                # 就是買的意思
                # 先查買價(用收盤價買)
                buy_price = self.table_features[stock].loc[date, 'close']
                if self.ex.buy(stock, self.spp.stock_name[stock], date, result[stock], buy_price):
                    print(date, "購買", stock, self.spp.stock_name[stock], "成功，剩餘", self.ex.money_quota)
                else:
                    print(date, "購買", stock, self.spp.stock_name[stock], "失敗!!!，剩餘", self.ex.money_quota)
            else:
                # 這裡就是賣囉
                # 要檢查是不是最後一天，因為隔天開盤才能賣
                # 賣價為隔天開盤價，原始版本的賣價為當天的開盤價應該是錯誤的
                next_day = self.table_features[stock].index.get_loc(date)
                if len(self.table_features[stock].index) <= next_day:
                    continue
                next_day += 1
                next_day = self.table_features[stock].index[next_day]
                sell_price = self.table_features[stock].loc[next_day, 'open']
                if self.ex.sell(stock, self.spp.stock_name[stock], date, -result[stock], sell_price):
                    print(date, "賣出", stock, self.spp.stock_name[stock], "成功，剩餘", self.ex.money_quota)
                else:
                    print(date, "賣出", stock, self.spp.stock_name[stock], "失敗!!!，剩餘", self.ex.money_quota)


if __name__ == '__main__':
    stock_list_path = 'D:/GoogleCloud/StockData2/stocknum.csv'
    # 底下就是直接用啦，輸入為(年, 月, 日, 股票清單, 總投資金額, 券商, 策略別稱)
    test = MainStructure(2019, 1, 2, stock_list_path, 1000000, '富邦', 'v01')
    test.activate()
