"""
This file can catch invest vander data of every stock

"""

import requests
import json
import datetime, time
import pandas as pd

todaydate = datetime.datetime.today()
YY = todaydate.year
MM = todaydate.month
DD = todaydate.day

investurl = ('http://www.twse.com.tw/fund/TWT44U?response=json&date='
             + str(YY) + str(MM) + str(DD))
foreignurl = ("http://www.twse.com.tw/fund/TWT38U?response=json&date="
              + str(YY) + str(MM) + str(DD))

class Investvander:


    def invest_buyin(investurl):

        html = requests.get(investurl)
        js = json.loads(html.text)
        if js['stat'] == 'OK':
            try:
                inv_dict = {}
                for row in js['data']:
                    del row[0]
                    inv_dict[row[0]] = row[1:]

                for key, values in inv_dict.items():
                    list = []
                    for idx in values:
                        b = idx.replace(",", "")
                        list.append(b)
                    inv_dict[key] = list
                df = pd.DataFrame(inv_dict).T

                return df

            except:
                pass

        else:
            return ""

#print(Investvander.invest_buyin(investurl))

    def foreign_buyin(foreignurl):

        html = requests.get(foreignurl)
        js = json.loads(html.text)
        if js['stat'] == 'OK':
            try:
                inv_dict = {}
                for row in js['data']:
                    del row[0]
                    inv_dict[row[0]] = row[1:]

                for key, values in inv_dict.items():
                    list = []
                    for idx in values:
                        b = idx.replace(",", "")
                        list.append(b)
                    inv_dict[key] = list
                df = pd.DataFrame(inv_dict).T

                return df.iloc[:,7:10]

            except:
                pass

        else:
            return ""

print(Investvander.foreign_buyin(foreignurl))