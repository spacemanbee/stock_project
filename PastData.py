"""

Past data of every stock
20190226 spaceman修改
直接執行本檔案後，會去網路上抓過去開市的K線股票資料
datatable的功能(依然保留)被拆成get_stock_data_by_date和get_stock_data_from_date_to_today

"""

import requests
import json
import datetime, time
import pandas as pd
from datetime import date
from urllib.request import urlopen
from bs4 import BeautifulSoup
import os
from twstock import realtime

# today = datetime.datetime.now()
# YY = today.year
# MM = today.month
# DD = today.day


class StockData:

    """url for function datatable"""

    first_url = "http://www.tse.com.tw/exchangeReport/MI_INDEX?response=json&date="
    last_url = "&type=ALLBUT0999&_=1496910989181"
    stock_columns = ["stockid", "stockname", "capacity",
                     "transaction", "turnover", "open", "high", "low",
                     "close", "1","2","3","4","5","6","7"]
    # docpath = "D:\stock_project\stockdata"
    # spaceman的路徑
    path1 = "C:/googlecloud/"
    # path1 = "D:/googlecloud/"   #sheman's path, please revise the path to yours
    path2 = "StockData2/"       #sheman's path, please revise the path to yours
    path3 = "realtimedata/"     #sheman's path, please revise the path to yours
    path4 = "everypiecedata/"   #sheman's path, please revise the path to yours
    path5 = "stockdata/"
    # dt_str = today.strftime("%Y%m%d")

    def get_stock_data_by_date(self, dt):
        # 先找儲存路徑中有沒有該檔案
        dt_str = dt.strftime("%Y%m%d")  # change the type of date into string
        file_name = self.path1 + self.path2 + self.path5 + dt_str+".json"
        if os.path.isfile(file_name):
            # print("檔案已存在")
            return 0
        # 檔案不存在的話再開始上網抓
        url = self.first_url + dt_str + self.last_url  # the url of the historic data
        html = requests.get(url)
        js = json.loads(html.text)

        if "data5" in js.keys():  # check whether the stock market is open or not
            df = pd.DataFrame(js['data5'], columns=self.stock_columns)  # this key(data5) includes all the stock data
            df.index = df['stockid']
            # docpath2 = os.path.join(self.path1, self.path2, self.path5, dt_str)
            with open(file_name, "w", encoding="utf-8") as datafile:
                df.to_json(datafile, force_ascii=False)
            return 1
        else:
            # print(dt, "本日未開盤")
            return -1

    def get_stock_data_from_date_to_today(self, year, month, day):
        # 這邊倒過來寫，從今天開始慢慢往回到指定日期
        dt = datetime.datetime.now()
        dt = date(dt.year, dt.month, dt.day)
        stop_date = date(year, month, day)  #the date we would like to catch data from
        while dt >= stop_date:
            result = self.get_stock_data_by_date(dt)
            print(dt, ": ", result)
            # result有三種結果，0表示檔案存在，1表示抓成功，-1表示沒開盤
            # 後兩者皆需要網路存取，所以都要time.sleep，免得被ban
            if result != 0:
                time.sleep(5)
            dt -= datetime.timedelta(days=1)

    def datatable(self, year, month, day):

        """
         Input:
         - year: year of data we would like to catch from
         - month: month of data we would like to catch from
         - day: day of data we would like to catch from

         Output:
         - json file which name is date is saved in the folder

        """
        today = datetime.datetime.now()
        YY = today.year
        MM = today.month
        DD = today.day

        dt = date(year, month, day)  #the date we would like to catch data from
        step = datetime.timedelta(days = 1)

        while dt <= date(YY, MM, DD):
            dt_str = dt.strftime("%Y%m%d")  #change the type of date into string
            url = self.first_url + dt_str + self.last_url  #the url of the historic data
            html = requests.get(url)
            js = json.loads(html.text)

            if "data5" in js.keys():  # check whether the stock market is open or not
                df = pd.DataFrame(js['data5'], columns=self.stock_columns)  # this key(data5) includes all the stock data
                df.index = df['stockid']
                docpath2 = os.path.join(self.path1, self.path2, self.path5, dt_str)
                with open(docpath2 + ".json", "w", encoding = "utf-8") as datafile:
                    df.to_json(datafile, force_ascii = False)
                    dt += step
                time.sleep(3)

            else:
                dt += step
                time.sleep(3)
                pass

        return ""

    def realtimedata(self):

        """
         Output
         - the json file of each stock's data, this function will catch realtime
         data of the stocks, each file uses stockid as file name, in the file,
         the data structure is dataframe

        """

        df = pd.read_csv("stocknum.csv")   #This file contains all stock id
        df1 = pd.DataFrame(df)


        for idx in range(len(df1['公司代號'])):

            stock_id = str(df1['公司代號'][idx])
            url = "https://www.cnyes.com/twstock/trade/" + stock_id + ".htm"
            html = urlopen(url).read().decode('utf-8')
            bs = BeautifulSoup(html, features="lxml")
            price_quan = bs.find_all("td", {"class": "lt"}) # catch the data we want


            docpath = os.path.join(self.path1, self.path2, self.path3, self.dt_str)
            if not os.path.exists(docpath):
                os.makedirs(docpath)
            docpath2 = os.path.join(docpath, stock_id)  # the file will save into this path

            price_list = []
            quantity_list = []

            for i in range(len(price_quan)):

                try:
                    if i % 3 == 0:
                        pricenum = "".join(filter(lambda x: x in '0123456789.', price_quan[i].text))
                        price_list.append(pricenum)

                    elif i % 3 == 1:
                        quantity_list.append(price_quan[i].text)

                except:
                    pass

            datajson = {"price": price_list, "quantity": quantity_list}
            df2 = pd.DataFrame(datajson)
            with open(docpath2 + ".json", "w", encoding="utf-8") as datafile:
                df2.to_json(datafile, force_ascii=False)
            time.sleep(2)

        return ""

    def everypiecedata(self,stockid):

        """
         Input:
         -  stockid: stock id as a string

         Output:
         - every piece of transaction data of stock, and the data is saved as
           json file

        """

        datalist = []
        data = realtime.get(stockid)

        if data["success"] == True: # make sure that the stock market is open

            for i in range(100):
                dict = {}
                data = realtime.get(stockid)    # get the newest data
                dict['time'] = data['info']['time']
                dict['latest_trade_price'] = data['realtime']['latest_trade_price']
                dict['trade_volume'] = data['realtime']['trade_volume']
                datalist.append(dict)
                time.sleep(5)
        else:
            pass

        docpath = os.path.join(self.path1, self.path2, self.path4, self.dt_str)
        if not os.path.exists(docpath):
            os.makedirs(docpath)
        docpath2 = os.path.join(docpath, stockid)

        df = pd.DataFrame(datalist, columns = ['time', 'latest_trade_price', 'trade_volume'])
        with open(docpath2 + ".json", "w", encoding = "utf-8") as datafile:
            df.to_json(datafile, force_ascii=False)
        time.sleep(2)

        return ""


Sto = StockData()
Sto.get_stock_data_from_date_to_today(2014, 1, 1)
# print(Sto.datatable(2018,10,10))
# print(Sto.realtimedata())
# print(Sto.everypiecedata("2345"))