"""
Ta_lib practice

"""


import talib
from talib import abstract
import pandas as pd
import datetime
import matplotlib.pyplot as plt
import twstock
from twstock import Stock
from InvestTable import Investvander

todaydate = datetime.datetime.today()
YY = todaydate.year
MM = todaydate.month
DD = todaydate.day

header_columns = ['date', 'capacity', 'turnover', 'open', 'high',
                  'low', 'close', 'change', 'transaction']

Inv = Investvander()
datatable = Inv.invest_buyin(2018,11,28)


def stock_close_price(datatable, rank):

    """
     Input:
     - datatable: invest vander buyin datatable(dataframe)
     - rank: number of stock (名次)

     Output:
     - stock close price for 31 days
    """

    stock = Stock((datatable.index)[rank])

    return stock.price

def stock_data(datatable, rank, year, month):

    """
     Input:
     - datatable: invest vander buyin datatable(dataframe)
     - rank: number of stock (名次)
     - year: the stock data from  year
     - month: the stock data from month

     Output:
     - all data of stock from (year, month) to now
    """

    stock = Stock((datatable.index)[rank])

    return stock.fetch_from(year, month)

def stockdata_table(datatable, rank, year, month):

    """
    Input :
     - datatable : the stock number which would be search
     - rank: number of stock (名次)
     - year : the data which would be collected from(year)
     - month : the data which would be collected from (month)

    Ourput:
    - df : the data array from year and month to now

    """

    table = stock_data(datatable, rank, year, month)
    df = pd.DataFrame(table) #把個股的資料整理成矩陣結構
    return df

print(stockdata_table(datatable, 5, 2018, 11))

# def close_price_trend_for_fivedays(stock, year, month):
#
#     table = stockdata_table(stock, year, month)
#     close_list = table['close']
#     print(talib.SMA(close_list, 5))

#print(close_price_trend_for_fivedays(stock, 2018, 5))

#close = np.random.random(100)
#print(talib.ADD(close)) #週期為3的移動平均
#today = datetime
#YY =
#MM =
#DD =

#df = pd.read_csv(filename)







