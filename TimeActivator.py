# -*- coding: utf-8 -*-
from datetime import datetime
from datetime import timedelta
import time
import sched


class TimeActivator:
    def __init__(self, start_time, stop_time):
        self.__sch__ = sched.scheduler(time.time, time.sleep)
        self.__MIN_REQUEST_INTERVAL__ = 3
        self.start_time = datetime.now().strftime("%Y-%m-%d") + " " + start_time
        self.stop_time = datetime.now().strftime("%Y-%m-%d") + " " + stop_time
        self.start_time = datetime.strptime(self.start_time, "%Y-%m-%d %H:%M:%S").time()
        self.stop_time = datetime.strptime(self.stop_time, "%Y-%m-%d %H:%M:%S").time()

    def re_run_func_by_interval(self, func_name, arg):
        try:
            func_name(*arg)
            self.__sch__.enter(self.__MIN_REQUEST_INTERVAL__, 2, self.re_run_func_by_interval, argument=(func_name, arg,))
        except Exception as e:
            print(e)

    def re_run_func_by_time_interval(self, func_name, arg):
        try:
            now_time = datetime.now().time()

            if self.start_time <= now_time <= self.stop_time:
                func_name(*arg)
                self.__sch__.enter(self.__MIN_REQUEST_INTERVAL__, 2, self.re_run_func_by_time_interval, argument=(func_name, arg,))
            else:
                pass
                # self.re_run_func_by_daytime(start_time, self.re_run_func_by_interval, [start_time, stop_time, arg])
        except Exception as e:
            print(e)

    def re_run_func_by_daytime(self, exe_time, func_name, arg):
        try:
            func_name(*arg)
            now = datetime.now()
            time.sleep(1)
            target = datetime.now().strftime("%Y-%m-%d") + " " + exe_time
            target = datetime.strptime(target, "%Y-%m-%d %H:%M:%S")
            if target < now:
                target += timedelta(days=1)
            time_interval_to_run = (target - now).seconds
            self.__sch__.enter(time_interval_to_run, 2, self.re_run_func_by_daytime, argument=(exe_time, func_name, arg,))
        except Exception as e:
            print(e.output)

    def activate(self):
        self.__sch__.run()

    def stop_event_by_daytime(self, event):
        self.__sch__.cancel(event)

    def show_timer_content(self):
        print(self.__sch__.queue)
