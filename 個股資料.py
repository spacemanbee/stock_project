"""
Ta_lib practice

"""

import numpy as np
import talib
from talib import abstract
import pandas as pd
import datetime
import matplotlib.pyplot as plt
import twstock
from twstock import Stock

stocknum = input('plz input the id number of stock:')
stock = Stock(str(stocknum))
header_columns = ['date', 'capacity', 'turnover', 'open', 'high',
                  'low', 'close', 'change', 'transaction']

def stock_close_price(stock):
    """
     Input : stock id_number
     Output : stock close price for 31 days
    """

    return stock.price

def stock_data(stock, year, month):
    """
     Input:
     - stock id_number

     Output:
     - all data of stock
    """

    return stock.fetch_from(year, month)
#print(stock_data(stock, 2018, 11))

def stockdata_table(stock, year, month):

    """
    Input :
    - stock : the stock number which would be search
    - year : the data which would be collected from(year)
    - month : the data which would be collected from (month)

    Ourput:
    - df : the data array from year and month to now

    """

    table = stock_data(stock, year, month)
    df = pd.DataFrame(table) #把個股的資料整理成矩陣結構
    return df

#print(stockdata_table(stock, 2018, 9))

def close_price_trend_for_fivedays(stock, year, month):

    table = stockdata_table(stock, year, month)
    close_list = table['close']
    print(talib.SMA(close_list, 5))

#print(close_price_trend_for_fivedays(stock, 2018, 5))

#close = np.random.random(100)
#print(talib.ADD(close)) #週期為3的移動平均
#today = datetime
#YY =
#MM =
#DD =

#df = pd.read_csv(filename)







