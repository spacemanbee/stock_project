"""
 KD line of TW50, find the difference of kd data to
decide when is the best buyin point

"""

from talib import abstract
import datetime
import pandas as pd
from datetime import date
import json
import talib

today = datetime.datetime.now()
YY = today.year
MM = today.month
DD = today.day

class StockMarketData():

    def __init__(self):

        self.STEP = datetime.timedelta(days=1)
        self.URL = ("https://goodinfo.tw/StockInfo/ShowK_Chart.asp?"
                    "STOCK_ID=%E5%8A%A0%E6%AC%8A%E6%8C%87%E6%95%B8&CHT_CAT2"
                    "=DATE=20190117")
        self.COLUMN = ["open", "close", "high", "low", "capacity"]

        # 這一個是抓取大盤資訊的URL,這裡暫時用不到，不過先留著
        #self.connect = "https://finance.yahoo.com/quote/%5ETWII/history?p=%5ETWII"

        # 這行是先把之前抓取的資料找出想追蹤的股票之後，整理成json格式
        self.stockjson = {}

        self.path1 = "D:/googlecloud/"  #sheman's path
        self.path2 = "StockData2/"      #sheman's path
        self.path3 = "stockdata/"       #sheman's path


    def stock_data_json(self, year, month, day, stocklist):

        """
         Input:
         - year : the year of historic data we catch
         - month : the month of historic data we catch
         - day : the day of historic data we catch
         - stocklist : 我們想追蹤的股票代碼(str)

         Output:
         - json structure of all stocks
            {stockid : { datetime : { 'open' : " " , 'close' : ' ', 'high': " ",
                       "low": " ", "capacity": " "}... }}
        """

        for stock in stocklist:
            dt = date(year, month, day)
            stockdict = {}
            while dt <= date(YY, MM, DD):
                pricedict = {}
                try:  # 有開盤就有資料
                    filename = dt.strftime("%Y%m%d") + ".json"
                    datapath = self.path1 + self.path2 + self.path3 + filename

                    with open(datapath, "r", encoding="utf-8") as f:
                        js = json.load(f)
                        for item in self.COLUMN:
                            # 把開盤收盤最高最低價全部整理dictionary,並且把數字裡的逗號拿掉
                            pricedict[item] = float(js[item][stock].replace(",", ""))
                        # {datetime: {open:" ", close: " ".....}
                        # 把每天的資料全部整理進去
                        stockdict[dt] = pricedict
                    dt += self.STEP

                except:  # 沒開盤就直接跳過
                    dt += self.STEP
                    pass
            # 股票代號是key, 每天資料的集合是value
            self.stockjson[stock] = stockdict

        return self.stockjson

    def stock_table(self, year, month, day, stocklist):

        """
         Input:
         - year : the year of historic data we catch
         - month : the month of historic data we catch
         - day : the day of historic data we catch
         - stocklist : 我們想追蹤的股票代碼(str)

         Output:
         - the dataframe of stock we choose, including all the information

        """

        tabledict = {}
        SMD = StockMarketData()

        # 呼叫function的json格式資料
        datatable = SMD.stock_data_json(year, month, day, stocklist)

        for stock in stocklist:

            # 轉成dataframe形式，將資料矩陣作轉秩動作
            df = pd.DataFrame(datatable[stock]).T

            # column多加了Date一行
            df['Date'] = df.index

            #將date裡的值從字串轉成datetime，如果要畫圖的話matplotlib就可以直接讀取
            df['Date'] = pd.to_datetime(df['Date'])

            #這裡要找出5日線,10日線,20日線
            ma5 = abstract.SMA(df,5)
            ma10 = abstract.SMA(df,10)
            ma20 = abstract.SMA(df,20)

            #在dataframe中多加入3行
            df['5MA'] = ma5
            df['10MA'] = ma10
            df['20MA'] = ma20

            #取得MACD技術資料
            df["MACD"],df["MACDsignal"],df["MACDhist"] = (talib.MACD(df["close"], fastperiod=12,
                                                                     slowperiod=26,signalperiod=9))
            df.dropna(how="any",inplace=True)  # 把dataframe裡的Na值全部改成0

            # key是股票代碼(str), value是dataframe形式的股票資料
            tabledict[stock] = df

        return tabledict

# print(StockMarketData.stock_table(2018,2018,3,6,["3596"]))


#class Tw50KDData(): #看最後兩個function就好
#
#     def __init__(self):
#         # 這行是先把之前抓取的資料找出台灣50之後，整理成json格式
#         self.TW50json = {}
#         # 大盤的資料整理之後弄成json格式
#         self.TWSTOCK = {}
#
#         # 這是我們需要的一些值
#         self.COLUMN = ["open", "close", "high", "low", "capacity"]
#         self.TWCOLUMN = ["Date", 'open', "close", "high", "low", 'volume']
#         self.date = []
#         self.open = []
#         self.close = []
#         self.high = []
#         self.low = []
#         self.adjclose = []
#         self.volume = []
#
#
#         self.STEP = datetime.timedelta(days=1)
#         self.URL = "https://goodinfo.tw/StockInfo/ShowK_Chart.asp?STOCK_ID=%E5%8A%A0%E6%AC%8A%E6%8C%87%E6%95%B8&CHT_CAT2=DATE=20190117"
#
#         #這一個是抓取大盤資訊的URL
#         self.connect = "https://finance.yahoo.com/quote/%5ETWII/history?p=%5ETWII"
#
#     def tw50_data_json(self, year, month, day):
#
#         """
#          Input:
#          - year : the year of historic data we catch
#          - month : the month of historic data we catch
#          - day : the day of historic data we catch
#
#          Output:
#          - json structure of TW50 data
#          { date : { 'open' : " " , 'close' : ' ', 'high': " ", "low": " ", "capacity": " "} }
#
#         """
#
#         dt = date(year, month, day)
#         while dt <= date(YY, MM, DD):
#
#             pricedict = {}
#             try:
#                 filename = dt.strftime("%Y%m%d")+".json" # 之前的歷史資料跟檔案放在同一層資料夾
#                 with open(filename, "r", encoding="utf-8") as f:
#                     js = json.load(f)
#                     for item in self.COLUMN:
#                         pricedict[item] = float(js[item]['0050'].replace(",", ""))
#                 self.TW50json[dt] = pricedict
#                 # 最後結構是以下這樣
#                 # { date : { 'open' : " " , 'close' : ' ', 'high': " ", "low": " ", "capacity": " "} }
#                 dt += self.STEP
#
#             except:
#                 dt += self.STEP
#                 pass
#
#         return self.TW50json
#
#     def tw50_datatable(self, year, month, day):
#
#         """
#          Input:
#          - year : the year of historic data we catch
#          - month : the month of historic data we catch
#          - day : the day of historic data we catch
#
#          Output:
#          - the dataframe of TW50, including all the information
#
#         """
#
#         TW50 = Tw50KDData()
#         #呼叫function的json格式資料
#         datatable = TW50.tw50_data_json(year, month, day)
#
#         #轉成dataframe形式
#         df = pd.DataFrame(datatable).T
#
#         #column多加了date一行
#         df['Date'] = df.index
#
#         #將date裡的值從字串轉成datetime，如果要畫圖的話matplotlib就可以直接讀取
#         df['Date'] = pd.to_datetime(df['Date'])
#
#         return df
#
#     def plot_stock_data(self, year, month, day):
#
#         """
#         Input:
#          - year : the year of historic data we catch
#          - month : the month of historic data we catch
#          - day : the day of historic data we catch
#
#         Output:
#          - the dataframe of TW50, including k_line and d-line
#
#         """
#
#
#         #呼叫TW50的dataframe
#         TW50 = Tw50KDData()
#         df = TW50.tw50_datatable(year, month, day)
#
#         #talib裡的abstract可以直接把dataframe的值整理成kd的dataframe形式
#         kd = abstract.STOCH(df)
#         # kd.plot(legend="best")
#         # plt.show()
#         k = kd.slowk
#         d = kd.slowd
#
#         return kd
#
#     def kd_add_to_table(self, year, month, day):
#
#         TW50 = Tw50KDData()
#         kdtable = TW50.plot_stock_data(year, month, day)
#         datatable = TW50.tw50_datatable(year, month, day)
#         dt = date(year, month, day)
#         datatable['k_value'] = kdtable.slowk
#         datatable['d_value'] = kdtable.slowd
#
#         return datatable
#
#     def stock_market_table(self,year, month, day):
#
#         TW50 = Tw50KDData()
#         datatable = TW50.tw50_datatable(year, month, day)
#
#         df = pd.read_csv("K_Chart.csv", encoding="big5")
#         df1 = (df.iloc[:202,:]).sort_index(ascending=False)
#
#         df1['date'] = datatable['Date'].values
#         df1.index = df1.date
#         kd = abstract.STOCH(df1)
#
#         df1['k_value'] = kd.slowk
#         df1['d_value'] = kd.slowd
#
#         datatable['k_value'] = kd.slowk
#         datatable['d_value'] = kd.slowd
#
#         return datatable
#
#     def twstock_datajson(self):
#
#         # 網路爬蟲取出日期和開收盤價及最高最低價
#         html = urlopen(self.connect).read().decode("utf-8")
#         bs = BeautifulSoup(html, features="lxml")
#         allnum = bs.find_all("td")
#         # 這行是要踢掉最後一個沒用的資訊
#         allnum.pop()
#
#         # 將各個價格整理到不同的列裡
#         for num in range(len(allnum)):
#
#             if num % 7 == 0:
#                 dt = pd.to_datetime(allnum[num].text)
#                 self.date.append(dt)
#             elif num % 7 == 1:
#                 a = allnum[num].text.replace(",", "")
#                 self.open.append(a)
#             elif num % 7 == 2:
#                 b = allnum[num].text.replace(",", "")
#                 self.high.append(b)
#             elif num % 7 == 3:
#                 c = allnum[num].text.replace(",", "")
#                 self.low.append(c)
#             elif num % 7 == 4:
#                 d = allnum[num].text.replace(",", "")
#                 self.close.append(d)
#             elif num % 7 == 5:
#                 e = allnum[num].text.replace(",", "")
#                 self.adjclose.append(e)
#             else:
#                 f = allnum[num].text.replace(",", "")
#                 self.volume.append(f)
#
#         # 整理成json格式
#         self.TWSTOCK['date'] = self.date
#         self.TWSTOCK["open"] = self.open
#         self.TWSTOCK['high'] = self.high
#         self.TWSTOCK['low'] = self.low
#         self.TWSTOCK['close'] = self.close
#         self.TWSTOCK['adjclose'] = self.adjclose
#         self.TWSTOCK['volume'] = self.volume
#
#         return self.TWSTOCK
#
#     def twstock_datatable(self):
#
#
#         TW50 = Tw50KDData()
#         # 呼叫function得到json格式的大盤資訊
#         stocktable = TW50.twstock_datajson()
#
#         # 轉成dataframe形式，並將各列的資訊轉成符點數型態
#         df = pd.DataFrame(stocktable, dtype="float64")
#         # 將日期的格式轉成datetime
#         df.date = pd.to_datetime(df.date)
#
#         # 將index轉換成datetime
#         df.index = df["date"]
#
#         # 踢掉不用的數列
#         df.drop(["adjclose", "volume"], axis=1, inplace=True)
#
#         # 照日期排序
#         df.sort_index(inplace=True)
#
#         # 自動計算大盤的kd值
#         kd = abstract.STOCH(df)
#
#         # 這裡擷取從8月30日到現在的0050的data是因為網站最早只提供8/30
#         df1 = TW50.tw50_datatable(2018,8,30)
#
#         # 在0050這支股票的dataframe裡加入k和d值的數列
#         df1['k_value'] = kd.slowk
#         df1['d_value'] = kd.slowd
#
#         return df1

# class BooleanBand():
#
#     def __init__(self):
#         self.STEP = datetime.timedelta(days=1)
#         self.URL = ("https://goodinfo.tw/StockInfo/ShowK_Chart.asp?"
#                     "STOCK_ID=%E5%8A%A0%E6%AC%8A%E6%8C%87%E6%95%B8&CHT_CAT2"
#                     "=DATE=20190117")
#         self.COLUMN = ["open", "close", "high", "low", "capacity"]
#
#         # 這一個是抓取大盤資訊的URL
#         self.connect = "https://finance.yahoo.com/quote/%5ETWII/history?p=%5ETWII"
#
#         # 這行是先把之前抓取的資料找出想追蹤的股票之後，整理成json格式
#         self.stockjson = {}
#
#         self.path1 = "D:/googlecloud/"
#         self.path2 = "StockData2/"
#         self.path3 = "stockdata/"
#
#     def stock_data_json(self, year, month, day, stocklist):
#
#         """
#          Input:
#          - year : the year of historic data we catch
#          - month : the month of historic data we catch
#          - day : the day of historic data we catch
#
#          Output:
#          - json structure of all stocks
#          {stockid : { datetime : { 'open' : " " , 'close' : ' ', 'high': " ",
#                     "low": " ", "capacity": " "}... }}
#
#         """
#
#         for stock in stocklist:
#
#             dt = date(year, month, day)
#             stockdict = {}
#             while dt <= date(YY, MM, DD):
#                 pricedict = {}
#                 try: # 有開盤就有資料
#                     filename = dt.strftime("%Y%m%d")+".json" # 之前的歷史資料跟檔案放在同一層資料夾
#                     datapath = self.path1 + self.path2 + self.path3 + filename
#                     with open(datapath, "r", encoding="utf-8") as f:
#                         js = json.load(f)
#                         for item in self.COLUMN:
#                             # 把開盤收盤最高最低價全部整理dictionary,並且把數字裡的逗號拿掉
#                             pricedict[item] = float(js[item][stock].replace(",", ""))
#                         # {datetime: {open:" ", close: " ".....}
#                         # 把每天的資料全部整理進去
#                         stockdict[dt] = pricedict
#
#                     dt += self.STEP
#
#
#                 except: # 沒開盤就直接跳過
#                     dt += self.STEP
#                     pass
#             # 股票代號是key, 每天資料的集合是value
#             self.stockjson[stock] = stockdict
#
#         return self.stockjson
#
#     def stock_table(self, year, month, day, stocklist):
#
#         """
#          Input:
#          - year : the year of historic data we catch
#          - month : the month of historic data we catch
#          - day : the day of historic data we catch
#
#          Output:
#          - the dataframe of stock we choose, including all the information
#
#         """
#
#         tabledict = {}
#         BB = BooleanBand()
#
#         # 呼叫function的json格式資料
#         datatable = BB.stock_data_json(year, month, day, stocklist)
#
#         for stock in stocklist:
#
#             # 轉成dataframe形式
#             df = pd.DataFrame(datatable[stock]).T
#
#             # column多加了date一行
#             df['Date'] = df.index
#
#             #將date裡的值從字串轉成datetime，如果要畫圖的話matplotlib就可以直接讀取
#             df['Date'] = pd.to_datetime(df['Date'])
#             # key是stockid, value是dataframe形式
#             tabledict[stock] = df
#
#         return tabledict
#
#     def booleanbands(self, year, month, day, stocklist):
#
#         BB = BooleanBand()
#         table = BB.stock_table(year, month, day, stocklist)
#         for stock in stocklist:
#
#             # 先取出收盤價
#             close_price = table[stock].close.values
#
#             # 把布林通道的上中下限一次算出來
#             # timeperiod是20日線設定, nbdevup上限是兩個標準差, nbdevdn=2是下限兩個標準差
#             upper, middle, lower = talib.BBANDS(close_price, timeperiod=20,
#                                           nbdevup=2,nbdevdn=2,matype=0)
#
#             table[stock]['bbupper'] = upper  # 在dataframe裡加入上限值
#             table[stock]['20MA'] = middle    # 在dataframe裡加入中間值(這條就是月線)
#             table[stock]['bblower'] = lower  # 在dataframe裡加入下限值
#             table[stock].fillna(0, inplace=True) # 把dataframe裡的Na值全部改成0
#
#         # 資料中多了上中下限的資料
#         return table

