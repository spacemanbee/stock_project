"""
foreign investment calculation

"""

import requests
import datetime
import json
import time

today_date = datetime.datetime.now()
YY = today_date.year
MM = today_date.month
DD = today_date.day

url = "http://www.twse.com.tw/fund/BFI82U?response=json&dayDate=" + str(YY)+str(MM)+"23"

def three_juristic_person_data(URL):
    """
     Input
     - URL : the linkage of data resource

     Output
     - dataform : data dictionary of three jurustic person per day

    """
    html = requests.get(URL)
    js = json.loads(html.text)
    if js['stat'] == 'OK':
        try:
            datalist = ['selfvander_invest', 'selfvander_risk',
                    '投信', 'foreign', 'sum']
            diff_list = []
            diff_dict = {}

            #header_dict = {'stat':'OK', 'title':'chinese of something',
            # 'fields':'["單位名稱","買進金額","賣出金額","買賣差額"]', 'date':'20181123', 'data'}
            for row in js['data']:
                num = ''
                for idx in row[3].split(','):
                   num += idx
                diff_list.append(int(num))

            for idx in range(len(diff_list)):
                diff_dict[datalist[idx]] = diff_list[idx]

            return diff_dict

        except:
            return ""
    else:
        return {}

#print(three_juristic_person_data(url))

def foreign_data_per_day(year, month, day):

    '''
     Input:
     - year : the year we wuold like to catch data from
     - month : the month we wuold like to catch data from
     - day : the day we wuold like to catch data from

     Output:
     - A list includes the foreign investment number

     '''

    datalist = ['selfvander_invest', 'selfvander_risk',
                '投信', 'foreign', 'sum']

    year_list = list(range(int(year), YY + 1))
    month_list = list(range(int(month), MM + 1))
    day_list = list(range(int(day), DD + 1))
    foreign_history_data_diff = []

    for years in year_list:
        for months in month_list:
            for days in day_list:

                if datetime.date(years, months, days) == datetime.datetime.now():
                    break

                else:
                    URL = ("http://www.twse.com.tw/fund/BFI82U?response=json&dayDate="
                       + str(years)+str(months)+str(days))
                    dataform = three_juristic_person_data(URL)
                    if 'foreign' not in dataform.keys():
                        pass
                    else:
                        foreign_history_data_diff.append(dataform[datalist[3]])
                        time.sleep(5)

    return foreign_history_data_diff

#print(foreign_data_per_day(2018, 11, 23))

def selfvanderinvest_data_per_day(year, month, day):
    '''
         Input:
         - year : the year we wuold like to catch data from
         - month : the month we wuold like to catch data from
         - day : the day we wuold like to catch data from

         Output:
         - A list includes the selfvander investment number

    '''

    datalist = ['selfvander_invest', 'selfvander_risk',
                '投信', 'foreign', 'sum']

    year_list = list(range(int(year), YY + 1))
    month_list = list(range(int(month), MM + 1))
    day_list = list(range(int(day), DD + 1))
    selfvanderinvest_history_data_diff = []

    for years in year_list:
        for months in month_list:
            for days in day_list:

                if datetime.date(years, months, days) == datetime.datetime.now():
                    break

                else:
                    URL = ("http://www.twse.com.tw/fund/BFI82U?response=json&dayDate="
                           + str(years) + str(months) + str(days))
                    dataform = three_juristic_person_data(URL)
                    if 'selfvander_invest' not in dataform.keys():
                        pass
                    else:
                        selfvanderinvest_history_data_diff.append(dataform[datalist[0]])
                        time.sleep(5)

    return selfvanderinvest_history_data_diff
#print(selfvanderinvest_data_per_day(2018, 11, 23))

def vander_data_per_day(year, month, day): #投信

    '''
         Input:
         - year : the year we wuold like to catch data from
         - month : the month we wuold like to catch data from
         - day : the day we wuold like to catch data from

         Output:
         - A list includes the vander investment number

    '''

    datalist = ['selfvander_invest', 'selfvander_risk',
                '投信', 'foreign', 'sum']

    year_list = list(range(int(year), YY + 1))
    month_list = list(range(int(month), MM + 1))
    day_list = list(range(int(day), DD + 1))
    vander_history_data_diff = []

    for years in year_list:
        for months in month_list:
            for days in day_list:

                if datetime.date(years, months, days) == datetime.datetime.now():
                    break

                else:
                    URL = ("http://www.twse.com.tw/fund/BFI82U?response=json&dayDate="
                           + str(years) + str(months) + str(days))
                    dataform = three_juristic_person_data(URL)
                    if '投信' not in dataform.keys():
                        pass
                    else:
                        vander_history_data_diff.append(dataform[datalist[2]])
                        time.sleep(5)

    return vander_history_data_diff
#print(vander_data_per_day(2018, 11, 23))