"""
This file can catch invest vander data of every stock

"""

import requests
import json
import datetime, time
import pandas as pd

todaydate = datetime.datetime.today()
YY = todaydate.year
MM = todaydate.month
DD = todaydate.day


class Investvander:

    investurl = 'http://www.twse.com.tw/fund/TWT44U?response=json&date='
    foreignurl = "http://www.twse.com.tw/fund/TWT38U?response=json&date="

    def invest_buyin(self, year, month, day):

        '''
         Input
         - year : the year of the data will be collected
         - month : the month of the data will be collected
         - day : the day of the data will be collected

         Output
         - an array with stockid as index, columns as difference of turnover
           values are data

        '''

        url = self.investurl + str(year) + str(month) + str(day)

        html = requests.get(url)
        js = json.loads(html.text)    #爬蟲台灣證交所的json資料
        if js['stat'] == 'OK':      #這行判斷當天是否有開盤
            try:
                inv_dict = {}
                for row in js['data']:
                    row[1] = row[1].rstrip(" ")   #資料有空格,把他整理乾淨
                    del row[0]
                    inv_dict[row[0]] = row[1:]    #把股票代碼當成key

                for key, values in inv_dict.items():
                    list = []
                    for idx in values:
                        b = idx.replace(",", "")
                        list.append(b)
                    inv_dict[key] = list
                df = pd.DataFrame(inv_dict)
                df.index = ['name', 'buyin_num',
                            'sellout_num', 'bs_num']
                dfreal = df.T

                return dfreal.ix[:, ['name', 'bs_num']]   #只擷取股票名稱和買超賣超總和

            except:
                pass

        else:
            return ""

# Inv = Investvander()
# print(Inv.invest_buyin(2018,11,28))

    def foreign_buyin(self, year, month, day):

        '''
         Input
         - year : the year of the data will be collected
         - month : the month of the data will be collected
         - day : the day of the data will be collected

         Output
         - an array with stockid as index, columns as difference of turnover
           values are data
        '''

        url = self.foreignurl + str(year) + str(month) + str(day)

        html = requests.get(url)
        js = json.loads(html.text)
        if js['stat'] == 'OK':
            try:
                inv_dict = {}
                for row in js['data']:
                    row[1] = row[1].rstrip(" ")
                    del row[0]
                    inv_dict[row[0]] = row[1:]

                for key, values in inv_dict.items():
                    list = []
                    for idx in values:
                        b = idx.replace(",", "")
                        list.append(b)
                    inv_dict[key] = list
                df = pd.DataFrame(inv_dict)
                df.index = ['name', 'buyin_num',
                            'sellout_num', 'bs_num1', 'buyin_num',
                            'sellout_num', 'bs_num2', 'buyin_num',
                            'sellout_num', 'bs_num3']
                dfreal = df.T

                return dfreal.ix[:, ['name', 'bs_num3']]

            except:
                pass

        else:
            return ""


#print(Investvander.foreign_buyin(foreignurl))










#     def invest_buyin_days(year, month, day):
#
#         yearlist = list(range(year, YY + 1))
#         monthlist = list(range(month, MM + 1))
#         daylist = list(range(day, DD + 1))
#
#         df1 = {}
#         for years in yearlist:
#             for months in monthlist:
#                 for days in daylist:
#
#                     if datetime.date(years, months, days) == todaydate:
#                         break
#
#                     url = ('http://www.twse.com.tw/fund/TWT44U?response=json&date='
#                            + str(years) + str(months) + str(days))
#
#                     html = requests.get(url)
#                     js = json.loads(html.text)
#                     if js['stat'] == 'OK':
#
#                         try:
#                             inv_dict = {}
#                             for row in js['data']:
#                                 row[1] = row[1].rstrip(" ")
#                                 del row[0]
#                                 inv_dict[row[0]] = row[1:]
#
#                             for key, values in inv_dict.items():
#                                 list = []
#                                 for idx in values:
#                                     b = idx.replace(",", "")
#                                     list.append(b)
#                                 inv_dict[key] = int(list[-1]) #key是股票代碼, value 是各成交量
#
#                             for key in inv_dict.keys():
#                                 if key not in df1.keys():
#                                     df1[key] = inv_dict[key]
#                                 else:
#                                     df1[key] += inv_dict[key]
#                             time.sleep(2)
#
#                         except:
#                             time.sleep(2)
#                             pass
#
#                     else:
#                         time.sleep(2)
#                         return ""
#
#         df = pd.DataFrame(df1)
#         df.index = ['bs_num']
#
#         return df.T
# print(Investvander.invest_buyin_days(2018,11,28))


