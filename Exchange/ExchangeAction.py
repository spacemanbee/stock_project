# 這裡提供買和賣的行為
# 如此就能在各自設計的買賣策略演算法中直接用這兩個行為(buy()和sell()兩個)就好
# ---------------怎麼用buy()和sell()---------------
# 1. from Exchange.ExchangeAction import ExchangeAction
# 2. 首先創建物件 xx = ExchangeAction(broker, money_quota)
#    money_quota請輸入一開始投資的金額
#    broker請輸入你在用的證券商
#    例如虛男用是富邦，然後決定要先投資100000，就 xx = ExchangeAction('富邦', 100000)
# 3. 在演算法決定要買的時候，用xx.buy('0050', '台灣50',  datetime.now(), 2, 100)
#    input依序為代碼、名稱、購買時間(請輸入datetime物件，不要用字串)、購買數量、單張金額(不好意思這功能並不具有查詢股價的功能，請自行填入正確數字)
#    單張金額請輸入乘以1000以前的，也就是一股的價格
# 4. 決定要賣的時候，用xx.sell('0050', '台灣50',  datetime.now(), 2, 100)
#    input順序和內容跟buy一樣
# 5. 詳細的交易紀錄會存在xx.table裡面，然後執行xx.sum_exchange_records()會統計所有交易紀錄
# 6. 測試結束後請合併資料xx.table.append(xx.sum_exchange_records())，然後呼叫xx.save_file()就可以將交易過程存檔
# 註：範例在第77行以後，包含五次交易
# ---------------用這個物件進行買賣有什麼好處呢--------------
# 1. 每次用buy或是sell都會當作一筆交易，記錄在檔案裡，路徑設定在ExchangeRecord.py裡面的 self.store_path = '../simResult/test1.json'(用瀏覽器開比較漂亮)
#    檔案裡面除了(a)每次交易的記錄之外，也會儲存(b)一開始投資金額，和(c)所有交易的統計資料
#    統計資料內容包含總投資資金MONEY_TRANSFER、剩餘金額RESIDUAL_MONEY、執行所有交易後各股狀態(持有數量HOLD、當前收支總計AMOUNT、買賣次數BUY和SELL)
# 2. 不用另外計算手續費啦，這兩個功能都有包含在裡面
# 3. 檢查買賣行為是否合法(例如買的時候會檢查剩餘資金夠不夠，賣的時候會檢查是否持有足夠數量的股票)

from Exchange.ExchangeRecord import ExchangeRecord
from Exchange.ExchangeRecord import Field
from Exchange.ExchangeResult import ExchangeResult


class ExchangeAction(ExchangeRecord):
    # broker在原本的class ExchangeRecord就有了，就是輸入證券商
    # money_quota表示新增的投資金額
    # path代表交易紀錄的檔案路徑
    # sim_start_date, sim_end_date是指模擬開始日期和結束日期
    # def __init__(self, broker, money_quota, sim_start_date, sim_end_date):
    def __init__(self, broker, money_quota):
        super().__init__(broker)
        # 這邊讀取某個已經存在的交易紀錄
        # 如果不存在表示是新的
        # self.sim_start_date = sim_start_date
        # self.sim_end_date = sim_end_date
        self.money_quota = money_quota
        self.record_quota()
        self.hold_table = dict()
        self.calc = ExchangeResult(broker)

    def record_quota(self):
        # 這裡先把東西塞進記憶體就好，不要寫檔
        content = {'MONEY_TRANSFER': self.money_quota}
        self.table.append(content)

    # 要記得檢查動作是否合法
    def buy(self, code, name, time, quantity, amount):
        if quantity < 1:
            return False
        # 先判斷合法
        # 執行store_an_exchange(self, sell, code, name, time, quantity, amount):
        aa = self.calc.total_pay_to_buy_stock(amount, quantity)
        if self.money_quota < aa:
            print("#ExAct: 錢不夠啦，剩", self.money_quota, '還想花', aa)
            return False
        self.store_an_exchange(-1, code, name, time, quantity, amount)
        # self.money_quota -= amount * 1000 * quantity
        self.money_quota -= aa
        self.table.append({Field.RESIDUAL.name: self.money_quota})
        if code in self.hold_table.keys():
            self.hold_table[code] += quantity
        else:
            self.hold_table[code] = quantity
        return True

    def sell(self, code, name, time, quantity, amount):
        if quantity < 1:
            return False
        if code not in self.hold_table.keys():
            print("#ExAct: 沒買過這張怎麼賣", code, name)
            return False
        if self.hold_table[code] < quantity:
            # print("#ExAct: 賣的數量大於持有量，持有", self.hold_table[code][Field.QUANTITY.name], '卻想賣', quantity)
            print("#ExAct: 賣的數量大於持有量，持有", self.hold_table[code], '卻想賣', quantity)
            return False
        self.store_an_exchange(1, code, name, time, quantity, amount)
        self.hold_table[code] -= quantity
        self.money_quota += self.calc.total_income_to_sell_stock(amount, quantity)
        self.table.append({Field.RESIDUAL.name: self.money_quota})
        return True


# t = ExchangeAction('富邦', 3100000)
# t.buy('0050', '台灣50',  datetime.now(), 2, 100)
# t.buy('0055', '台灣五百', datetime.now(), 100, 10)
# t.sell('0050', '台灣50', datetime.now(), 1, 100)
# t.sell('0050', '台灣50', datetime.now(), 1, 100)
# t.sell('0055', '台灣五百', datetime.now(), 100, 10)
# t.table.append(t.sum_exchange_records())
# t.save_file()


