# from twstock import Stock
# import datetime

# 這個class主要在計算買賣股票時的實際費用，當然包含手續費計算
# ----------用法-----------
# 假設一股交易價500元
# 1. 先宣告物件 ex = ExchangeResult('xxx')   xxx請輸入券商名稱(請看RATE_DISCOUNT)
# 買的時候呼叫這個  ex.total_income_to_sell_stock(500) 會回傳賣出後總得到金額
# 賣的時候呼叫這個  ex.total_pay_to_buy_stock(500) 會回傳買進總共需支出金額

# 買賣股票的手續費
# 目前券商規則，每筆買或賣交易成交後，券商可收 成交金額之千分之1.425 為手續費，小數點以下無條件捨去，
# 之後依各家券商規定，不足20元以20元計
# 另需再付 千分之三 證券交易稅
# 買股票時需付 券商手續費(千分之1.425)
# 賣股票時需付 券商手續費(千分之1.425) + 政府證券交易稅(千分之3)
# --------買股票範例-----------
# 假設今日買 1 塊股票一張，成交金額 1,000，
# 需付券商手續費 1.425 元，小數點以下捨去為 1 元，因不足20元以20元計
# 因此需付 1020 元，必須注意即使券商有手續費打折，也仍需計手續費20元
# 成交金額 + 券商手續費 = 付出金額
# 1000 + 20 = 1020
# --------賣股票範例---------
# 假設今日賣 1 塊股票一張，成交金額 1000，
# 需付券商手續費 1.425 元，小數點以下捨去為 1 元，因不足20元以20元計
# 因此可得到 1000 元，再繳20予券商，另外還要付證券交易稅 3 元
# 成交金額 - 券商手續費 - 證券交易稅 = 得到金額
# 1000 – 20 – 3 = 977
# --------有關券商的手續費打折---------
# http://dreamingbug.pixnet.net/blog/post/26562554-%E5%90%84%E5%AE%B6%E8%AD%89%E5%88%B8%E5%95%86%E6%89%8B%E7%BA%8C%E8%B2%BB
# --------變數命名----------
# broker是券商的意思
# gov是政府的意思
# income: 收入
# pay: 支出


class ExchangeResult:
    FEE_BROKER_RATE = 0.001425
    FEE_GOV_RATE = 0.003
    RATE_DISCOUNT = {'元大': 0.6, '兆豐': 0.6, '1040': 0.35, '臺銀': 0.35, 'test': 0.28, '富邦': 0.6, '台新': 0.3}
    FEE_MIN = 20 #大部分是20TWD，有需要再改

    def __init__(self, broker: str):
        self.brokerRateDiscount = 1
        if broker in self.RATE_DISCOUNT.keys():
            self.brokerRateDiscount = self.RATE_DISCOUNT[broker]
        self.buyPrice = 0
        self.result = 0
        self.stocks = {}

    # 以下改為每支股票的總交易金額來算
    # 就是假設有一張5元的股票，我想買100張
    # 舊的算法是先以每張股票20元手續費來算(因為5000*0.001425 < 20)，所以就是2000元
    # 但實際上應該是拿總金額來算5000*100*0.001425 = 712.5元才對
    # 所以頻繁的低價交易也是浪費錢
    def fee_broker(self, price, number):
        return round(max(self.brokerRateDiscount * self.FEE_BROKER_RATE * price * 1000 * number, self.FEE_MIN))

    def fee_gov(self, price, number):
        return round(self.FEE_GOV_RATE * price * 1000 * number)

    # 輸入價錢，最後會回傳總共支出金額
    # buy_price請輸入一股價錢(會自動幫你x1000)
    def total_pay_to_buy_stock(self, buy_price, number):
        result = buy_price * 1000 * number + self.fee_broker(buy_price, number)
        return result

    # 輸入價錢，最後會回傳總共收入金額
    # sell_price請輸入一股價錢(會自動幫你x1000)
    def total_income_to_sell_stock(self, sell_price, number):
        result = sell_price * 1000 * number - self.fee_broker(sell_price, number) - self.fee_gov(sell_price, number)
        return result

    def max_number_can_buy(self, money, stock_price):
        # 手續費有max不太好求，所以用點智障方法
        # 先算出最大值，再慢慢遞減就好
        number = int(money/stock_price)
        while money < self.total_pay_to_buy_stock(stock_price, number):
            number -= 1
        return number

    # def buy_stock(self, stock: Stock, buyday: datetime.datetime, number: int):
    #     buyPrice = stock.low[stock.date.index(buyday)]
    #     print(str(buyPrice))
    #     self.result -= (buyPrice*1000 + self.fee_broker(buyPrice))*number
    #     print(['total:', str(self.result)])
    #     if stock.sid in self.stocks:
    #         self.stocks[stock.sid] += number
    #     else:
    #         self.stocks[stock.sid] = number
    #
    # def sell_stock(self, stock: Stock, sellday: datetime.datetime, number: int):
    #     sellPrice = stock.high[stock.date.index(sellday)]
    #     print(str(sellPrice))
    #     self.result += (sellPrice*1000 + self.fee_broker(sellPrice) + self.fee_gov(sellPrice))*number
    #     print(str(self.result))
    #     self.stocks[stock.sid] -= number


# ex = ExchangeResult('富邦')
# print(ex.total_income_to_sell_stock(1, 1))
# print(ex.total_pay_to_buy_stock(1, 100))
