# 想要定義交易紀錄表格
# 每一筆交易資料大概有以下資訊
# 欄位一 SELL: 買入(-1)或賣出(1)(這樣統計就可以直接用乘的啦)
# 欄位二 CODE: 股票代碼
# 欄位三 NAME: 股票名稱
# 欄位四 TIME: 交易日期時間(格式用datetime)
# 欄位五 QUANTITY: 交易數量(用張數計算)
# 欄位六 AMOUNT: 單張交易金額(應該就是成交價吧，用TWD)
from enum import Enum
from datetime import datetime
import json
from Exchange import ExchangeResult as calc


class Field(Enum):
    SELL = 'SELL'
    CODE = 'CODE'
    NAME = 'NAME'
    TIME = 'TIME'
    QUANTITY = 'QUANTITY'
    AMOUNT = 'AMOUNT'
    MONEY_TRANSFER = 'MONEY_TRANSFER'
    RESIDUAL = 'RESIDUAL'


class RField(Enum):
    MONEY_TRANSFER = 'MONEY_TRANSFER'
    RESIDUAL_MONEY = 'RESIDUAL_MONEY'
    CODE = 'CODE'
    NAME = 'NAME'
    BUY = 'BUY'
    SELL = 'SELL'
    HOLD = 'HOLD'
    AMOUNT = 'AMOUNT'


class ExchangeRecord:
    def __init__(self, broker):
        self.table = list()
        # self.store_path = 'D:/stock_project/simResult/test1.json'
        self.store_path = 'simResult/test1.json'
        self.broker = broker

    def store_an_exchange(self, sell, code, name, time, quantity, amount):
        record = dict()
        record[Field.SELL.name] = sell
        record[Field.CODE.name] = code
        record[Field.NAME.name] = name
        record[Field.TIME.name] = time.strftime("%Y%m%d-%H%M%S")
        record[Field.QUANTITY.name] = quantity
        record[Field.AMOUNT.name] = amount
        self.table.append(record)

    def save_file(self):
        f = open(self.store_path, 'a+')
        f.write(json.dumps(self.table))
        return self.store_path

    def load_file_as_dict(self):
        f = open(self.store_path, 'r')
        self.table = json.loads(f.read())
        # return temp

    def sum_exchange_records(self):
        # 這個函式統計table裡面的所有資料，算出賺賠(當然要包含檢查這張交易紀錄的對錯)
        # 股票代碼:{股票名稱、剩餘數量、總花費、買入次數、賣出次數}
        # 後面兩個次數只統計下單次數不包含張數資訊
        result = dict()
        residual = 0
        # 要分開每個股票的賺賠
        for record in self.table:
            c = calc.ExchangeResult(self.broker)
            if Field.RESIDUAL.name in record.keys():
                continue
            if Field.MONEY_TRANSFER.name in record.keys():
                if Field.MONEY_TRANSFER.name not in result.keys():
                    result[Field.MONEY_TRANSFER.name] = record[Field.MONEY_TRANSFER.name]
                else:
                    result[Field.MONEY_TRANSFER.name] += record[Field.MONEY_TRANSFER.name]
                residual += record[Field.MONEY_TRANSFER.name]
                continue
            # 先處理買入的部分
            if record[Field.SELL.name] == -1:
                temp = c.total_pay_to_buy_stock(record[Field.AMOUNT.name], record[Field.QUANTITY.name])
                if residual < temp:
                    print('#3 騙鬼喔錢不夠怎麼買', record[Field.NAME.name], '，你只剩', residual, '卻想花', temp)
                    print(record)
                    return
                residual -= temp
                if record[Field.CODE.name] in result.keys():
                    result[record[Field.CODE.name]][RField.AMOUNT.name] -= temp
                    result[record[Field.CODE.name]][RField.HOLD.name] += record[Field.QUANTITY.name]
                    result[record[Field.CODE.name]][RField.BUY.name] += 1
                else:
                    result[record[Field.CODE.name]] = dict()
                    result[record[Field.CODE.name]][RField.NAME.name] = record[Field.NAME.name]
                    result[record[Field.CODE.name]][RField.HOLD.name] = record[Field.QUANTITY.name]
                    result[record[Field.CODE.name]][RField.AMOUNT.name] = -temp
                    result[record[Field.CODE.name]][RField.BUY.name] = 1
                    result[record[Field.CODE.name]][RField.SELL.name] = 0
            # 再來是賣出的部分
            elif record[Field.SELL.name] == 1:
                if record[Field.CODE.name] not in result.keys():
                    print('#1 無買入此股票', record[Field.NAME.name])
                    return
                hold = result[record[Field.CODE.name]][RField.HOLD.name] - record[Field.QUANTITY.name]
                if hold < 0:
                    print('#2 持有張數不足')
                    return
                result[record[Field.CODE.name]][RField.HOLD.name] = hold
                temp = c.total_income_to_sell_stock(record[Field.AMOUNT.name], record[Field.QUANTITY.name])
                result[record[Field.CODE.name]][RField.AMOUNT.name] += temp
                result[record[Field.CODE.name]][RField.SELL.name] += 1
                residual += temp
        ans = result[Field.MONEY_TRANSFER.name]
        for i in result:
            if RField.MONEY_TRANSFER.name in i:
                continue
            ans += result[i][RField.AMOUNT.name]
        result[RField.RESIDUAL_MONEY.name] = ans
        return result

#
# test = ExchangeRecord('富邦')
# test.load_file_as_dict()
# # print(test.table)')
# # # test_time = datetime.now()
# # # test.store_an_exchange(-1, '0050', '台灣50', test_time, 2, 100)
# # # test.store_an_exchange(-1, '0055', '台灣50', test_time, 100, 10)
# # # test.store_an_exchange(1, '0050', '台灣50', test_time, 1, 20)
# # # test.store_an_exchange(1, '0050', '台灣50', test_time, 1, 40)
# # # test.store_an_exchange(1, '0055', '台灣50', test_time, 90, 40)
# # # name = test.save_file(
# print(test.sum_exchange_records())
